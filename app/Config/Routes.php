<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

//$routes->group("api", function ($routes) {

//    $routes->post("register", "User::register");
//    $routes->post("login", "User::login");
//    $routes->get("profile", "User::details");
//});

// ****** SELF SERVICES API ******
$routes->resource('customeruser');
$routes->resource('customerorder');
$routes->resource('customerordertelephone');
$routes->resource('customerorderaddon');
$routes->resource('customercompanies');
$routes->resource('customeraddress');
$routes->resource('customeraddresscategory');
$routes->resource('location');

$routes->resource('obdrule');
$routes->resource('ibpbx');
$routes->resource('didnumber');
$routes->resource('user');
$routes->resource('unique');

// ****** MAILER API ******
$routes->post("mailersmtp/send_invoice", "MailerSmtp::send_invoice");
$routes->post("mailersmtp/send_credential", "MailerSmtp::send_credential");
$routes->post("mailersmtp/send_sales_notification", "MailerSmtp::send_sales_notification");

// ****** VOIPSWITCH API ******
$routes->resource('subtrunk');
$routes->get('Voipswitch/show_subtrunk', 'Voipswitch::show_subtrunk');
$routes->post('Voipswitch/update_subtrunk', 'Voipswitch::update_subtrunk');
$routes->post('Voipswitch/create_pbx_account', 'Voipswitch::create_pbx_account');
$routes->post('Voipswitch/add_balance', 'Voipswitch::add_balance');
$routes->post('Voipswitch/assign_did_number', 'Voipswitch::assign_did_number');
// $routes->post('Voipswitch/sync_account', 'Voipswitch::sync_account');
// $routes->resource('voipswitch');

// ****** DOKU PAYMENT API ******
// $routes->add('payment/doku/invoice','PaymentDoku::payment_invoice');
$routes->add('payment/doku/invoice/(:any)','PaymentDoku::payment_invoice/$1');
$routes->add('payment/doku/redirect','PaymentDoku::redirect');
$routes->add('payment/doku/identify','PaymentDoku::identify');
$routes->add('payment/doku/notify','PaymentDoku::notify');
$routes->add('payment/doku/create_payment','PaymentDoku::create_payment');

//  ****** ADMIN DEV API  ******
$routes->delete("admin/delete_customer", "Admin::delete_customer");

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
