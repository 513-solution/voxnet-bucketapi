<?php

namespace App\Controllers;
use App\Models\UserModel;
use App\Models\CustomerUserModel;
use App\Models\CustomerCompaniesModel;
use App\Models\CustomerAddressModel;
use App\Models\CustomerOrderModel;
use App\Models\CustomerOrderAddonModel;
use App\Models\CustomerOrderTelephoneModel;
use App\Models\MasterModel;
use CodeIgniter\RESTful\ResourceController;

class Admin extends ResourceController
{
	public function __construct()
	{
        $this -> user = new UserModel();
		$this -> customerUser = new CustomerUserModel();
		$this -> customerCompanies = new CustomerCompaniesModel();
		$this -> customerAddress = new CustomerAddressModel();
		$this -> customerOrder = new CustomerOrderModel();
		$this -> customerOrderAddon = new CustomerOrderAddonModel();
		$this -> customerOrderTelephone = new CustomerOrderTelephoneModel();
		$this -> master = new MasterModel();

		date_default_timezone_set('Asia/Jakarta');
    }

    public function check_user_permission($username = null, $password = null)
    {
        if($username && $password)
        {
            $userData = $this->user
            ->where(['username' => $username])
                ->where(['password' => md5($password)])
                ->orWhere(['password' => $password])
                ->select('permission')
              ->first();
    
            if(empty($userData))
            {
                return null;
            }

            return $userData['permission']; 
        }
        return null;

    }

    // For Dev Purposes
    // public function check_point($data = null)
    // {
    //     $response = [
    //         'status' => 200,
    //         'messages' => 'CHECK POINT!',
    //         'data' => $data,
    //     ];
    //     return $this->respond($response);
    // }

    public function delete_customer()
    {
        $username = $this->request->getVar('username');
		$password = $this->request->getVar('password');
        $permission = null;

        $permission = $this->check_user_permission($username, $password);

        if($permission == '1')
        {
            $cc_id = $this->request->getVar('cc_id');

            // if cc_id is 'last', get cc_id of last company
            if($cc_id == 'last') {
                $companies_length = $this->customerCompanies->countAll();
                $all_companies = $this->customerCompanies->findAll();
                $last_company = $all_companies[$companies_length - 1];
                $cc_id = $last_company['cc_id'];
            }

            
            // check if cc_id not exist
            $company_cc_id = $this->customerCompanies->where(['cc_id' => $cc_id])->first();

            if(empty($company_cc_id))
            {
                return $this->respondNoContent('cc_id not found');
            }

            try{
                // get co_id(s)
                foreach($this->customerOrder->where(['cc_id' => $cc_id])->findAll() as $order)
                {
                    $co_id = $order['co_id'];
                    // delete customer_order_telephone
                    foreach( $this->customerOrderTelephone->where(['co_id' => $co_id])->findAll() as $cot)
                    {
                        $cot_id = $cot["cot_id"];
                        
                        $this->customerOrderTelephone->delete(['cot_id' => $cot_id]);
                    }
                    // delete customer_order_addon
                    foreach( $this->customerOrderAddon->where(['co_id' => $co_id])->findAll() as $coa)
                    {
                        $coa_id = $coa["coa_id"];
                        $this->customerOrderAddon->delete(['coa_id' => $coa_id]);
                    }
                    // delete customer_order
                    $this->customerOrder->delete(['co_id' => $co_id]);
                }
                // get all cu_name_email(s) to delete at user table
                foreach($this->customerUser->where(['cc_id' => $cc_id])->findAll() as $cu)
                {
                    $cu_name_email = $cu['cu_name_email'];
                    // delete user(s)
                    $user_id = $this->user->where(['username' => $cu_name_email])->first();
                    if(!empty($user_id))
                    {
                        $data = $user_id['user_id'];
                        $this->user->delete(['user_id' => $user_id['user_id']]);
                    }
                }
                // delete customer_user(s)
                foreach( $this->customerUser->where(['cc_id' => $cc_id])->findAll() as $cu)
                {
                    $cu_id = $cu["cu_id"];
                    $this->customerUser->delete(['cu_id' => $cu_id]);
                }
                // delete customer_address
                foreach( $this->customerAddress->where(['cc_id' => $cc_id])->findAll() as $ca)
                {
                    $ca_id = $ca["ca_id"];
                    $this->customerAddress->delete(['ca_id' => $ca_id]);
                }
                // get company name
                $cc_name = $this->customerCompanies->where(['cc_id' => $cc_id])
                            ->select('cc_name')->first();
                // delete customer_companies
                $this->customerCompanies->delete(['cc_id' => $cc_id]);
                $response = [
                    'status' => 200,
                    'error' => '',
                    'messages' => "customer '".$cc_name['cc_name']."' with cc_id ".$cc_id." deleted!",
                ];
                return $this->respondDeleted($response);

            } catch (\Throwable $th) {
                return $this->fail("Something Wrong When Deleting A Customer", 400);
			}
        }
        else {
            return $this->failUnauthorized('Not Allowed');
        }
        
    }

    // public function tomd5($password = null)
	// {
	// 	$hashedPassword = md5($password);
		
	// 	$response = [
	// 		'md5' => $hashedPassword,
	// 	];

	// 	return $this->respond($response);
	// }
}
