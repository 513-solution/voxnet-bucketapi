<?php

namespace App\Controllers;
use App\Models\CustomerAddressModel;
use CodeIgniter\RESTful\ResourceController;

class CustomerAddress extends ResourceController
{

	public function index()
	{
		$model = new CustomerAddressModel();
      
        $data = $model->findAll();
      
        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer Address Found",
            "data" => $data,
        ];
        return $this->respond($response);
	}

	public function show($id = null)
	{
		$model = new CustomerAddressModel();
      
        $data = $model->where(['ca_id' => $id])->first();
      
        if ($data) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Customer Address Found",
                "data" => $data,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No Customer Address Found with id ' . $id);
        }
	}

	public function new()
	{
		//
	}

	public function create()
	{
		$model = new CustomerAddressModel();

        $data = [
            'cc_id' => $this->request->getVar('cc_id'),
            'cac_id' => $this->request->getVar('cac_id'),
			'ca_attention' => $this->request->getVar('ca_attention'),
            'ca_address_1' => $this->request->getVar('ca_address_1'),
			'ca_address_2' => $this->request->getVar('ca_address_2'),
            'ca_lr_id' => $this->request->getVar('ca_lr_id'),
			'ca_lp_id' => $this->request->getVar('ca_lp_id'),
            'ca_zipcode' => $this->request->getVar('ca_zipcode'),
			'ca_fax' => $this->request->getVar('ca_fax'),
            'ca_country' => $this->request->getVar('ca_country'),
        ];

        $model->insert($data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer Address Saved",
        ];
      
        return $this->respondCreated($response);
	}

	public function edit($id = null)
	{
		//
	}

	public function update($id = null)
	{
		$model = new CustomerAddressModel();

        $data = [
            'cc_id' => $this->request->getVar('cc_id'),
            'cac_id' => $this->request->getVar('cac_id'),
			'ca_attention' => $this->request->getVar('ca_attention'),
            'ca_address_1' => $this->request->getVar('ca_address_1'),
			'ca_address_2' => $this->request->getVar('ca_address_2'),
            'ca_lr_id' => $this->request->getVar('ca_lr_id'),
			'ca_lp_id' => $this->request->getVar('ca_lp_id'),
            'ca_zipcode' => $this->request->getVar('ca_zipcode'),
			'ca_fax' => $this->request->getVar('ca_fax'),
            'ca_country' => $this->request->getVar('ca_country'),
        ];

        $model->update($id, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Data Updated"
        ];
        return $this->respond($response);
	}

	public function delete($id = null)
	{
		$model = new CustomerAddressModel();

        $data = $model->find($id);

        if ($data) {

            $model->delete($id);

            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Data Deleted",
            ];
            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound('No Data Found with id ' . $id);
        }
	}
}
