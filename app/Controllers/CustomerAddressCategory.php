<?php

namespace App\Controllers;
use App\Models\CustomerAddressCategoryModel;
use CodeIgniter\RESTful\ResourceController;

class CustomerAddressCategory extends ResourceController
{

	public function index()
	{
		$model = new CustomerAddressCategoryModel();
      
        $data = $model->findAll();
      
        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer Address Category Found",
            "data" => $data,
        ];
        return $this->respond($response);
	}

	public function show($id = null)
	{
		$model = new CustomerAddressCategoryModel();
      
        $data = $model->where(['cac_id' => $id])->first();
      
        if ($data) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Customer Address Category Found",
                "data" => $data,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No Customer Address Category Found with id ' . $id);
        }
	}

	public function new()
	{
		//
	}

	public function create()
	{
		$model = new CustomerAddressCategoryModel();

        $data = [
            'cac_name' => $this->request->getVar('cac_name'),
            'cac_default' => $this->request->getVar('cac_default'),
        ];

        $model->insert($data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer Address Category Saved",
        ];
      
        return $this->respondCreated($response);
	}

	public function edit($id = null)
	{
		//
	}

	public function update($id = null)
	{
		$model = new CustomerAddressCategoryModel();

        $data = [
            'cac_name' => $this->request->getVar('cac_name'),
            'cac_default' => $this->request->getVar('cac_default'),
        ];

        $model->update($id, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Data Updated"
        ];
        return $this->respond($response);
	}

	public function delete($id = null)
	{
		$model = new CustomerAddressCategoryModel();

        $data = $model->find($id);

        if ($data) {

            $model->delete($id);

            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Data Deleted",
            ];
            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound('No Data Found with id ' . $id);
        }
	}
}
