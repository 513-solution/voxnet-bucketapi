<?php

namespace App\Controllers;
use App\Models\CustomerCompaniesModel;
use CodeIgniter\RESTful\ResourceController;

class CustomerCompanies extends ResourceController
{
	public function index()
	{
		$model = new CustomerCompaniesModel();
      
        $data = $model->findAll();
      
        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer Companies Found",
            "data" => $data,
        ];
        return $this->respond($response);
	}

	public function show($id = null)
	{
		$model = new CustomerCompaniesModel();
      
        $data = $model->where(['cc_id' => $id])->first();
      
        if ($data) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Customer Companies Found",
                "data" => $data,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No Customer Companies Found with id ' . $id);
        }
	}
    
	public function new()
	{
		//
	}

	public function create()
	{
		$model = new CustomerCompaniesModel();

        $data = [
            'cc_name' => $this->request->getVar('cc_name'),
            'cc_phone' => $this->request->getVar('cc_phone'),
            'cc_web' => $this->request->getVar('cc_web'),
            'cc_npwp' => $this->request->getVar('cc_npwp'),
        ];

        $cc_id = $model->insert($data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer Companies Saved",
            'data' => [
                'cc_id' => $cc_id
            ]
        ];
      
        return $this->respondCreated($response);
	}

	public function edit($id = null)
	{
		//
	}

	public function update($id = null)
	{
		$model = new CustomerCompaniesModel();

        $data = [
            'cc_name' => $this->request->getVar('cc_name'),
            'cc_phone' => $this->request->getVar('cc_phone'),
            'cc_web' => $this->request->getVar('cc_web'),
            'cc_npwp' => $this->request->getVar('cc_npwp'),
        ];

        $model->update($id, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Data Updated"
        ];
        return $this->respond($response);
	}

	public function delete($id = null)
	{
		$model = new CustomerCompaniesModel();

        $data = $model->find($id);

        if ($data) {

            $model->delete($id);

            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Data Deleted",
            ];
            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound('No Data Found with id ' . $id);
        }
	}
}
