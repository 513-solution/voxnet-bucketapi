<?php

namespace App\Controllers;
use App\Models\CustomerOrderModel;
use CodeIgniter\RESTful\ResourceController;

class CustomerOrder extends ResourceController
{

	public function index()
	{
		$model = new CustomerOrderModel();
      
        $data = $model->findAll();
      
        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer Order Found",
            "data" => $data,
        ];
        return $this->respond($response);
	}

	public function show($id = null)
	{
		$model = new CustomerOrderModel();
      
        $data = $model->where(['co_id' => $id])->first();
      
        if ($data) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Customer Order Found",
                "data" => $data,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No Customer Order Found with id ' . $id);
        }
	}

	public function new()
	{
		//
	}

	public function create()
	{
		$model = new CustomerOrderModel();

        $data = [
            'p_id' => $this->request->getVar('p_id'),
            'cc_id' => $this->request->getVar('cc_id'),
            'co_num_ip' => $this->request->getVar('co_num_ip'),
			'co_num_inv' => $this->request->getVar('co_num_inv'),
            'co_num_transdate' => $this->request->getVar('co_num_transdate'),
            'co_num_transcode' => $this->request->getVar('co_num_transcode'),
			'co_num_transidmerchant' => $this->request->getVar('co_num_transidmerchant'),
            'co_name' => $this->request->getVar('co_name'),
            'co_name_telnumber' => $this->request->getVar('co_name_telnumber'),
			'co_name_trunknum' => $this->request->getVar('co_name_trunknum'),
            'co_name_talkcredit' => $this->request->getVar('co_name_talkcredit'),
			'co_name_maxsub' => $this->request->getVar('co_name_maxsub'),
			'co_name_maxtalkcredit' => $this->request->getVar('co_name_maxtalkcredit'),
            'co_name_monthlysubscribe' => $this->request->getVar('co_name_monthlysubscribe'),
			'co_status' => $this->request->getVar('co_status'),
        ];

        $co_id = $model->insert($data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer Order Saved",
            'data' => [
                'co_id' => $co_id,
            ]
        ];
      
        return $this->respondCreated($response);
	}

	public function edit($id = null)
	{
		//
	}

	public function update($id = null)
	{
		$model = new CustomerOrderModel();

        $data = [
            'p_id' => $this->request->getVar('p_id'),
            'cc_id' => $this->request->getVar('cc_id'),
            'co_num_ip' => $this->request->getVar('co_num_ip'),
			'co_num_inv' => $this->request->getVar('co_num_inv'),
            'co_num_transdate' => $this->request->getVar('co_num_transdate'),
            'co_num_transcode' => $this->request->getVar('co_num_transcode'),
			'co_num_transidmerchant' => $this->request->getVar('co_num_transidmerchant'),
            'co_name' => $this->request->getVar('co_name'),
            'co_name_telnumber' => $this->request->getVar('co_name_telnumber'),
			'co_name_trunknum' => $this->request->getVar('co_name_trunknum'),
            'co_name_talkcredit' => $this->request->getVar('co_name_talkcredit'),
            'co_name_talkdur' => $this->request->getVar('co_name_talkdur'),
			'co_name_maxsub' => $this->request->getVar('co_name_maxsub'),
			'co_name_maxtalkcredit' => $this->request->getVar('co_name_maxtalkcredit'),
            'co_name_avgsubscribe' => $this->request->getVar('co_name_avgsubscribe'),
            'co_name_monthlysubscribe' => $this->request->getVar('co_name_monthlysubscribe'),
			'co_status' => $this->request->getVar('co_status'),
            'co_createdate' => $this->request->getVar('co_createdate'),
            'co_activedate' => $this->request->getVar('co_activedate'),
        ];

        $model->update($id, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Data Updated"
        ];
        return $this->respond($response);
	}

	public function delete($id = null)
	{
		$model = new CustomerOrderModel();

        $data = $model->find($id);

        if ($data) {

            $model->delete($id);

            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Data Deleted",
            ];
            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound('No Data Found with id ' . $id);
        }
	}
}
