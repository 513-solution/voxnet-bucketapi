<?php

namespace App\Controllers;
use App\Models\CustomerOrderAddonModel;
use CodeIgniter\RESTful\ResourceController;

class CustomerOrderAddon extends ResourceController
{
	public function index()
	{
		$model = new CustomerOrderAddonModel();
      
        $data = $model->findAll();
      
        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer Order Addon Found",
            "data" => $data,
        ];
        return $this->respond($response);
	}

	public function show($id = null)
	{
		$model = new CustomerOrderAddonModel();
      
        $data = $model->where(['coa_id' => $id])->first();
      
        if ($data) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Customer Order Addon Found",
                "data" => $data,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No Customer Order Addon Found with id ' . $id);
        }
	}

	public function new()
	{
		//
	}

	public function create()
	{
		$model = new CustomerOrderAddonModel();

        $data = [
            'co_id' => $this->request->getVar('co_id'),
            'pa_id' => $this->request->getVar('pa_id'),
            'pam_id' => $this->request->getVar('pam_id'),
            'coa_value' => $this->request->getVar('coa_value'),
			'coa_price' => $this->request->getVar('coa_price'),
            'coa_qty' => $this->request->getVar('coa_qty'),
            'coa_price_total' => $this->request->getVar('coa_price_total'),
        ];

        $model->insert($data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer Order Addon Saved",
        ];
      
        return $this->respondCreated($response);
	}

	public function edit($id = null)
	{
		//
	}

	public function update($id = null)
	{
		$model = new CustomerOrderAddonModel();

        $data = [
            'co_id' => $this->request->getVar('co_id'),
            'pa_id' => $this->request->getVar('pa_id'),
            'pam_id' => $this->request->getVar('pam_id'),
            'coa_value' => $this->request->getVar('coa_value'),
			'coa_price' => $this->request->getVar('coa_price'),
            'coa_qty' => $this->request->getVar('coa_qty'),
            'coa_price_total' => $this->request->getVar('coa_price_total'),
        ];

        $model->update($id, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Data Updated"
        ];
        return $this->respond($response);
	}

	public function delete($id = null)
	{
		$model = new CustomerOrderAddonModel();

        $data = $model->find($id);

        if ($data) {

            $model->delete($id);

            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Data Deleted",
            ];
            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound('No Data Found with id ' . $id);
        }
	}
}
