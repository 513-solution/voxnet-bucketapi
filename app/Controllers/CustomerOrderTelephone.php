<?php

namespace App\Controllers;
use App\Models\CustomerOrderTelephoneModel;
use App\Models\ProductDIDNumberModel;
use CodeIgniter\RESTful\ResourceController;

class CustomerOrderTelephone extends ResourceController
{
	public function __construct()
	{
		  $this -> customerOrderTelephone = new CustomerOrderTelephoneModel();
      $this -> productDIDNumber = new ProductDIDNumberModel();
	}


	public function index()
	{      
        $data = $this->customerOrderTelephone->findAll();
      
        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer Order Found",
            "data" => $data,
        ];
        return $this->respond($response);
	}


	public function show($id = null)
	{      
        $data = $this->customerOrderTelephone->where(['cot_id' => $id])->first();
      
        if ($data) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Customer Order Telephone Found",
                "data" => $data,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No Customer Order Telephone Found with id ' . $id);
        }
	}

	public function create()
	{
			$data = [
				'co_id' => $this->request->getVar('co_id'),
				'cot_city' => $this->request->getVar('cot_city'),
				'cot_telephone' => $this->request->getVar('cot_telephone'),
			];
			
			$cot_telephones = $data['cot_telephone'];
			$cot_telephones = explode(',', $cot_telephones);

			foreach($cot_telephones as $cot_telephone)
			{

				$this->productDIDNumber
				->set('pdn_status', 'cancelled')
				->where('pdn_did_number', $cot_telephone)
				->update();
			}

			$this->customerOrderTelephone->insert($data);

			$response = [
				'status' => 200,
				'error' => null,
				'messages' => "Customer Order Telephone Saved",
			];
		  
			return $this->respondCreated($response);
	}

}
