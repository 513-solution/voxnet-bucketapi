<?php

namespace App\Controllers;
use App\Models\CustomerUserModel;
use CodeIgniter\RESTful\ResourceController;

class CustomerUser extends ResourceController
{
	public function index()
	{
		$model = new CustomerUserModel();
      
        $data = $model->findAll();
      
        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer User Found",
            "data" => $data,
        ];
        return $this->respond($response);
	}

	public function show($id = null)
	{
		$model = new CustomerUserModel();
      
        $data = $model->where(['cu_id' => $id])->first();
      
        if ($data) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Customer User Found",
                "data" => $data,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No Customer User Found with id ' . $id);
        }
	}

	public function new()
	{
		//
	}

	public function create()
	{
		$model = new CustomerUserModel();

        $data = [
            'cc_id' => $this->request->getVar('cc_id'),
            'cu_name_first' => $this->request->getVar('cu_name_first'),
            'cu_name_last' => $this->request->getVar('cu_name_last'),
			'cu_name_dsp' => $this->request->getVar('cu_name_dsp'),
            'cu_name_email' => $this->request->getVar('cu_name_email'),
            'cu_name_mobile' => $this->request->getVar('cu_name_mobile'),
        ];

        $model->insert($data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Customer User Saved",
        ];
      
        return $this->respondCreated($response);
	}


	public function edit($id = null)
	{
		//
	}


	public function update($id = null)
	{
		$model = new CustomerUserModel();

        $data = [
			'cc_id' => $this->request->getVar('cc_id'),
            'cu_name_first' => $this->request->getVar('cu_name_first'),
            'cu_name_last' => $this->request->getVar('cu_name_last'),
			'cu_name_dsp' => $this->request->getVar('cu_name_dsp'),
            'cu_name_email' => $this->request->getVar('cu_name_email'),
            'cu_name_mobile' => $this->request->getVar('cu_name_mobile'),
        ];

        $model->update($id, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Data Updated"
        ];
        return $this->respond($response);
	}

	public function delete($id = null)
	{
		$model = new CustomerUserModel();

        $data = $model->find($id);

        if ($data) {

            $model->delete($id);

            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Data Deleted",
            ];
            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound('No Data Found with id ' . $id);
        }
	}
}
