<?php

namespace App\Controllers;
use App\Models\DidnumberModel;
use App\Models\PutPBXDialingplan;
use App\Models\GetiduserModel;
use CodeIgniter\RESTful\ResourceController;

class Didnumber extends ResourceController
{

	public function show($login = null)
	{
        $model = new GetiduserModel();
        $data = $model->getWhere(['login' => $login])->getResult();
        if ($data) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "ID Username Found",
                "data" => $data,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No ID Username Found with login ' . $login);
        }
	}

	public function create()
	{
	    $model = new DidnumberModel();

        $data = [
            'client_id' => $this->request->getVar('client_id'),
            'country_id' => $this->request->getVar('country_id'),
	        'client_type' => $this->request->getVar('client_type'),
            'area_code' => $this->request->getVar('area_code'),
	        'area_name' => $this->request->getVar('area_name'),
            'phone_number' => $this->request->getVar('phone_number'),
        ];

        $model->insert($data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Did Number Added",
        ];
      
        return $this->respondCreated($response);
	}

}

