<?php

namespace App\Controllers;
use App\Models\IbpbxModel;
use App\Models\IbobgetModel;
use CodeIgniter\RESTful\ResourceController;

class Ibpbx extends ResourceController
{


	public function show($login = null)
	{
		$model = new IbobgetModel();
        $data = $model->getWhere(['login' => $login])->getResult();
        if ($data) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Found",
                "data" => $data,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('Not Found ' . $login);
        }
	}

	public function create()
	{
		$model = new IbpbxModel();

        $data = [
            'telephone_number' => $this->request->getVar('telephone_number'),
            'priority' => $this->request->getVar('priority'),
	    'route_type' => $this->request->getVar('route_type'),
            'tech_prefix' => $this->request->getVar('tech_prefix'),
            'id_route' => $this->request->getVar('id_route'),
	    'call_type' => $this->request->getVar('call_type'),
	    'type' => $this->request->getVar('type'),
            'balance_share' => $this->request->getVar('balance_share'),
	    'call_limit' => $this->request->getVar('call_limit'),
	    'id_dial_plan' => $this->request->getVar('id_dial_plan'),
        ];

        $model->insert($data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Dialplan Added",
        ];
      
        return $this->respondCreated($response);
	}

}

