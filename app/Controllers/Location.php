<?php

namespace App\Controllers;
use App\Models\LocatRegenciesModel;
use App\Models\LocatProvincesModel;
use CodeIgniter\RESTful\ResourceController;

class Location extends ResourceController{

  public function __construct()
	{
		$this -> locatProvinces = new locatProvincesModel();
    $this -> locatRegencies = new locatRegenciesModel();
	}

  public function Provinces(){
    $provinces = $this->locatProvinces->findAll();

    if ($provinces) {
      $response = [
          'status' => 200,
          'error' => null,
          'messages' => "Product Found",
          "data" => $provinces,
      ];
      return $this->respond($response, 200);
    } else {
      return $this->failNotFound('No Province Found');
    }
  }

  public function Regencies($province_id = null){
    $regencies = $this->locatRegencies->where(['province_id' => $province_id])->findAll();

    if ($regencies) {
      $response = [
          'status' => 200,
          'error' => null,
          'messages' => "Product Found",
          "data" => $regencies,
      ];
      return $this->respond($response, 200);
    } else {
      return $this->failNotFound('No Regency Found');
    }
  }



}