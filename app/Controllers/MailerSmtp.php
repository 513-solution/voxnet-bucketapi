<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use App\Models\MasterModel;

class MailerSmtp extends ResourceController
{
	public function __construct()
	{
		$this->master = new MasterModel();

		helper('url');
		helper('html');
		helper('form');
	}

	public function send_invoice()
	{
		$template    = $this->request->getVar('template');
		$destination = $this->request->getVar('destination');
		$co_id       = $this->request->getVar('co_id');
		$sip_login = $this->request->getVar('sip_login');
		$username = $this->request->getVar('username');
		$password = $this->request->getVar('password');
		$pending_activation = $this->request->getVar('pending_activation');

		// Verify template
		$allowed_templates = ['invoice', 'pi', 'credential'];

		if(!in_array($template, $allowed_templates))
		{
			return $this->fail("only 'invoice', 'pi', and 'credential' template is allowed!");
		}

		// Verify destination email
		if(!filter_var($destination, FILTER_VALIDATE_EMAIL)) {
  			return $this->fail('Incorrect Destination Email Format!');
		}
		
		// Email Config
		$email = \Config\Services::email();
		$email->setTo($this->request->getVar('destination'));
		$email->setFrom('noreply@voxnet.id');

		try {
				$data['pending_activation'] = $pending_activation;
				// Get Data for Email Subject and Body
				$subject = '';
				if($template == 'pi')
				{
					$data['doc_title'] = 'Proforma Invoice';
					$subject = 'Proforma Invoice';
					
				}
				if($template == 'invoice')
				{
					$subject = 'Invoice';
					$data['doc_title'] = 'Invoice';

					if($data['pending_activation'])
					{
						$subject = 'Pembayaran Anda Sudah Kami Terima';
					}
				}
				if($template == 'credential')
				{
					$subject = 'Selamat, Akun Voxnet Anda Sudah Aktif!';
					$data['doc_title'] = 'Invoice';

					if(!$password || !$username || !$sip_login)
					{
						return $this->fail("Provide username, sip_login and password!", 400);
					}
					$data['sip_login'] = $sip_login;
					$data['username'] = $username;
					$data['password'] = $password;
				}
			
				$email->setSubject($subject);
				$data['template'] = $template;
				$data['ppn'] = 10;
				// $data['doc_footer'] = 'Sesuai dengan Peraturan Jendral Pajak Nomor PER-13/PJ/2019 tanggal 2 Juli 2019 tagihan ini berfungsi sebagai dokumen yang dipersamakan dengan Faktur Pajak.';
				$data['doc_footer'] = '';
				$data['co_id'] = $co_id;

				if(($template == 'invoice' || $template == 'pi') && !$co_id)
				{
					return $this->fail("Provide co_id for PI and invoice!", 400);
				}
				
				
				// Get Customer Order Datas for Order Table
				if($co_id)
				{	
					// Check if co_id exist in customer_order table
					$customer_order_detail = $this->master->get_row_param('customer_order', 'co_id', $co_id);
					if(empty($customer_order_detail))
					{
						return $this->fail("co_id not found!", 400);
					}

					$data['customer_order_detail'] = $customer_order_detail;
					
					$whereAddress = array(
						//testing
						'cc_id' => $data['customer_order_detail']['cc_id'],
						'cac_id' => 1
					);
	
					$data['default_address'] = $this->master->get_min_where_array('ca_id', 'customer_address', $whereAddress);
					$data['customer_companies_detail']	= $this->master->get_row_param('customer_companies', 'cc_id', $whereAddress['cc_id']);
					$data['customer_address_default']	= $this->master->get_row_param('customer_address', 'ca_id', $data['default_address']['ca_id']);
					$data['customer_address_city']		= $this->master->get_row_param('locat_regencies', 'id', $data['customer_address_default']['ca_lr_id']);
					$data['customer_address_province']	= $this->master->get_row_param('locat_provinces', 'id', $data['customer_address_default']['ca_lp_id']);
					$data['customer_order_addon']		= $this->master->get_result_param('customer_order_addon', 'co_id', $co_id);
					$data['subs_total'] = $data['customer_order_detail']['co_name_monthlysubscribe'] * $data['customer_order_detail']['co_mo_subs'];

					$invSt = $data['customer_order_detail']['co_num_inv'];
					$invNd = $data['customer_order_detail']['co_num_transdate'];
					$invTh = $data['customer_order_detail']['co_num_transcode'];
					$invFt = $data['customer_order_detail']['co_num_transidmerchant'];

					$data['customer_order_detail']['doku_payment_link'] = '';

					if($template == 'pi')
					{
						$data['customer_order_detail']['doku_payment_link'] = 'https://bucketapi.voxnet.id/public/payment/doku/invoice/'.$invFt."";	
					}

					$co_createdate	= date_create($data['customer_order_detail']['co_createdate']);
					$co_activedate	= date_create($data['customer_order_detail']['co_activedate']);
					$co_expireddate	= date_create($data['customer_order_detail']['co_expireddate']);
	
					$data['period']	= date_format($co_activedate, "d-m-Y") . " - " . date_format($co_expireddate, "d-m-Y");
					
					// Set invoice code prefix (PI/.... or INV/.... )
					if(isset($data['customer_order_detail']['co_num_inv']) && $template != 'pi')
					{
						$invSt = $data['customer_order_detail']['co_num_inv'];
					} else {
						$invSt = $data['customer_order_detail']['co_num_ip'];
					}

					$data['period']	= date_format($co_createdate, "d-m-Y");
					$arrayInv = array($invSt, $invNd, $invTh, $invFt);
	
					$data['invoice'] = implode('/', $arrayInv);
				}
				
				// Determine Email Body

				$email_body = "";

				$email_body = view('mailer/mailer_invoice', $data);

				$email->setMessage($email_body);
				
				// Send Email
				if ($email->send()) {
					$response = [
						'status' => 200,
						'error' => null,
						'messages' => $template." email sent to ".$destination,
					];
					return $this->respond($response);
				} else {
					return $this->fail("Something Wrong When Sending Email", 500);;
				}
	
			} catch (\Throwable $th) {
				return $this->fail("Something Wrong When Composing Email", 500);;
			}	
	}
	
	public function send_credential()
	{
		$destination = $this->request->getVar('destination');
		$sip_login   = $this->request->getVar('sip_login');
		$username    = $this->request->getVar('username');
		$password    = $this->request->getVar('password');

		if(!$password || !$username || !$sip_login)
		{
			return $this->fail("Provide username, sip_login and password!", 400);
		}

		if(!$destination)
		{
			return $this->fail("Provide destination email!", 400);
		}

		$data = [
			'sip_login' => $sip_login,
			'username' => $username,
			'password' => $password,
		];
		
		$email_body = '';
		$email_body = view('mailer/mailer_credential', $data);
		
		// Email Config
		$email = \Config\Services::email();
		$email->setTo($this->request->getVar('destination'));
		$email->setFrom('noreply@voxnet.id');
		$email->setSubject('Voxnet SIP Login Credential');
		$email->setMessage($email_body);
		

		// Send Email
		if ($email->send()) {
			$response = [
				'status' => 200,
				'error' => null,
				'messages' => "credential email re-sent!",
			];
			return $this->respond($response);
		} else {
			return $this->fail("Something Wrong When Sending Email", 500);;
		}
	}

	public function send_sales_notification()
	{
		$destination = $this->request->getVar('destination');
		$co_id = $this->request->getVar('co_id');
		$subject = $this->request->getVar('subject');
		$content = $this->request->getVar('content');
		$error_msg = $this->request->getVar('error_msg');

		if(!$subject){
			$subject = 'Gagal Aktivasi Akun PBX/SIP';
		}

		$subject = '[Test Automated Register] '.$subject;
		
		if(!in_array($content, ['failed_activation', 'failed_did'])){
			$content = 'failed_activation';	
		} 
		
		$data['content'] = $content;

		if(!$error_msg){
			$error_msg = '-';
		}
		
		$data['error_msg'] = $error_msg;


		if(!$destination || !$co_id)
		{
			return $this->fail("Provide destination email and co_id!", 400);
		}

		// Get Customer Order Datas for Order Table
		if($co_id)
		{	
			$data['doc_title'] = 'Invoice';
			$data["co_id"] = $co_id;
			
			// Check if co_id exist in customer_order table
			$customer_order_detail = $this->master->get_row_param('customer_order', 'co_id', $co_id);
			if(empty($customer_order_detail))
			{
				return $this->fail("co_id not found!", 400);
			}

			$data['customer_order_detail'] = $customer_order_detail;
			
			$whereAddress = array(
				//testing
				'cc_id' => $data['customer_order_detail']['cc_id'],
				'cac_id' => 1
			);

			// Customer Address

				// Initiate Address Value
			$data['customer_address_city']['name'] = '';
			$data['customer_address_province']['name'] ='';

			$data['default_address'] = $this->master->get_min_where_array('ca_id', 'customer_address', $whereAddress);
			$data['customer_companies_detail']	= $this->master->get_row_param('customer_companies', 'cc_id', $whereAddress['cc_id']);
			$data['customer_address_default']	= $this->master->get_row_param('customer_address', 'ca_id', $data['default_address']['ca_id']);
			$data['customer_address_city']		= $this->master->get_row_param('locat_regencies', 'id', $data['customer_address_default']['ca_lr_id']);
			$data['customer_address_province']	= $this->master->get_row_param('locat_provinces', 'id', $data['customer_address_default']['ca_lp_id']);
			$data['customer_order_addon']		= $this->master->get_result_param('customer_order_addon', 'co_id', $co_id);
			$data['subs_total'] = $data['customer_order_detail']['co_name_monthlysubscribe'] * $data['customer_order_detail']['co_mo_subs'];

			$invSt = $data['customer_order_detail']['co_num_inv'];
			$invNd = $data['customer_order_detail']['co_num_transdate'];
			$invTh = $data['customer_order_detail']['co_num_transcode'];
			$invFt = $data['customer_order_detail']['co_num_transidmerchant'];

			$co_createdate	= date_create($data['customer_order_detail']['co_createdate']);
			$co_activedate	= date_create($data['customer_order_detail']['co_activedate']);
			$co_expireddate	= date_create($data['customer_order_detail']['co_expireddate']);

			$data['period']	= date_format($co_activedate, "d-m-Y") . " - " . date_format($co_expireddate, "d-m-Y");
			$data['doc_footer'] = '';
			
			//CUSTOMER ORDER TELEPHONE
			$data['customer_order_telephone'] = $this->master->get_result_param('customer_order_telephone', 'co_id', $co_id);
			// Set invoice code prefix (PI/.... or INV/.... )
			$invSt = $data['customer_order_detail']['co_num_inv'];
			if(!$invSt)
			{
				$invSt = $data['customer_order_detail']['co_num_ip'];
			}	
			
			$data['period']	= date_format($co_createdate, "d-m-Y");
			$arrayInv = array($invSt, $invNd, $invTh, $invFt);

			$data['invoice'] = implode('/', $arrayInv);
		}
		
		$email_body = '';
		$email_body = view('mailer/mailer_notification.php', $data);
		
		// Email Config
		$email = \Config\Services::email();
		$email->setTo($this->request->getVar('destination'));
		$email->setFrom('noreply@voxnet.id');
		$email->setSubject($subject);
		$email->setMessage($email_body);
		

		// Send Email
		if ($email->send()) {
			$response = [
				'status' => 200,
				'error' => null,
				'messages' => $content." notification email sent to ".$destination,
			];
			return $this->respond($response);
		} else {
			return $this->fail("Something Wrong When Sending Email", 500);;
		}
	}
}
