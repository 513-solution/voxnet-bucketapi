<?php

namespace App\Controllers;
use App\Models\ObgetModel;
use CodeIgniter\RESTful\ResourceController;

class ObDRule extends ResourceController
{

	public function show($login = null)
	{
		$model = new ObgetModel();
        $data = $model->getWhere(['login' => $login])->getResult();
        if ($data) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Found",
                "data" => $data,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No ID Client ' . $login);
        }
	}

    public function update($id_client = null)
	{
		$model = new ObgetModel();
        $data = $model->where(['id_client' => $id_client])->first();
        $data = [
            'tech_prefix' => $this->request->getVar('tech_prefix'),
        ];

        $model->update($id_client, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Dial Rules Added"
        ];
        return $this->respond($response);
	}

}

