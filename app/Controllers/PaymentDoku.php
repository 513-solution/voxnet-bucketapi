<?php

namespace App\Controllers;
use App\Models\CustomerOrderModel;
use App\Models\CustomerOrderAddonModel;
use App\Models\CustomerCompaniesModel;
use App\Models\CustomerOrderTelephoneModel;
use App\Models\PaymentDokuModel;
use App\Models\CurlModel;
use App\Models\UserModel;
use App\Models\MasterModel;

use App\Controllers\Voipswitch;
use CodeIgniter\RESTful\ResourceController;

class PaymentDoku extends ResourceController
{
	public function __construct()
	{
		$this->url = 'https://staging.doku.com/Suite/Receive';
        $this->mallid = 9299;
        $this->sharedKey = 'LVF6tu6NIT1X';
        $this->curlModel = new CurlModel();
        $this->master = new MasterModel();
        $this->VoxnetBaseUrl = 'https://bucketapi.voxnet.id/public';
        date_default_timezone_set('Asia/Jakarta');
        $this->client = \Config\Services::curlrequest();
        $this->Voipswitch = new Voipswitch();
	}

	public function checkpoint($data)
	{
		$response = [
			'data' => $data
		];
		return $this->respond($response);
	}

   	public function payment_invoice($invoice){
   		// Check From doku only maxium 1 times same Invoice
   		$paymentDoku = new PaymentDokuModel();
   		$paymentDoku->where('status','FAILED');
   		$countDokuFailed = $paymentDoku->where(['co_transIdMerchat' => $invoice])->findAll();
   		if (count($countDokuFailed) == 1) {
			$data['message'] = 'Payment Failed';
			echo view('doku/failed_payment', $data);
   		} else {
   			$customerOrderModel = new CustomerOrderModel();
	   		$customerOrderModel->where("DATE(co_createdate) > DATE_SUB(NOW(), INTERVAL 24 HOUR)");
	   		$customerOrderModel->where('co_status','1');
	        $data = $customerOrderModel->where(['co_num_transidmerchant' => $invoice])->first();
	        
	        if ($data) {
	        	$url = $this->url;
		    	$mallid = $this->mallid;
		    	$sharedKey = $this->sharedKey;

	        	// Get Data Customer
	        	$customerCompaniesModel = new CustomerCompaniesModel();
	        	$customerDetail = $customerCompaniesModel->where(['cc_id' => $data['cc_id']])->first();
	        	// Get price from customer Order
	        	$getPriceProduct = $data['co_name_monthlysubscribe'];
				// get orderAddon 
				$customerAddon = new CustomerOrderAddonModel();
				$customerAddon->join('product_addon pa','pa.pa_id=customer_order_addon.pa_id');
				$customerAddon->join('product_addon_matrix pam','pam.pam_id=customer_order_addon.pam_id');
	        	$pDetail = $customerAddon->where(['co_id'=>$data['co_id']])->findAll();
	        	$exAddon = array();
	        	$exTotalPriceAddon = array();
	        	for ($i=0; $i <count($pDetail) ; $i++) { 
	        		// "PPN".','.$priceIncTax.'.00,1,'.$priceIncTax.'.00';
	        		// Add-On SubAccounts: 1 1 50,000.00 50,000.00 4 200,000.00
	        		$addonItemName = $pDetail[$i]['pa_name'];
	        		$addonMatrix = $pDetail[$i]['pam_value'];
	        		$addonPrice = $pDetail[$i]['coa_price'];
	        		$addonQty = $pDetail[$i]['coa_qty'];
	        		
	        		if ($pDetail[$i]['coa_mo_subs'] != null) {
	        			$addonMonth = $pDetail[$i]['coa_mo_subs'];
	        		} else {
	        			$addonMonth = $data['co_mo_subs'];
	        		}
	        		$totalPriceAddon = ($addonQty*$addonPrice)*$addonMonth;
	        		$buildAddon = $addonItemName.' '.$addonMatrix.','.$totalPriceAddon.'.00,'.$addonMonth.','.$totalPriceAddon.'.00';
	        		array_push($exAddon, $buildAddon);
	        		array_push($exTotalPriceAddon,$totalPriceAddon);
	        	}
	        	// Push Addon to basket
	        	$addonJoin = join(";",$exAddon);
	        	$totalAddonSum = array_sum($exTotalPriceAddon);
	        	
			    	$currency = '360';
			    	$transactionId = $invoice;
			    	$itemName = $data['co_name'];
					$email = 'customer@voxnet.id';
					$name = 'Voxnet Payment Doku';
					// $name = $customerDetail['cc_name'];
					$address= 'JL.TB.Simatupang No.19-22 RT.001 RW.04,Rambutan,Jakarta Timur';
					$homephone = '62218413592';
					$zipcode = '13830';
					$requestDateTime = date('YmdHis');
			    	$sessionId = $this->randomString(20); //set value to fetch from doku later for validating the transaction
			    	$paymentChanel = '';

			    	$quantityItem = $data['co_mo_subs'];
			    	$orderPrice = ($quantityItem*$getPriceProduct);

			    	$priceIncTax=($totalAddonSum+$orderPrice)*10/100;
			    	$amount = $orderPrice+$totalAddonSum+$priceIncTax.'.00';
			    	
					$orderBasket = $itemName.','.$getPriceProduct.','.$quantityItem.','.$orderPrice.'.00';
					$taxBasket = "PPN".','.$priceIncTax.'.00,1,'.$priceIncTax.'.00';
					if ($addonJoin) {
						$generateBasket = $orderBasket.';'.$addonJoin.';'.$taxBasket;
					} else {
						$generateBasket = $orderBasket.';'.$taxBasket;
					}
					
					$generateWord = sha1($amount.$mallid.$sharedKey.$transactionId); 

		    	$parameters = [
		    		'URL'				=> $url,
					'BASKET'			=> $generateBasket,
					'MALLID'			=> $mallid,
					'CHAINMERCHANT'		=> 'NA', //Given by Doku. If not using Chain, use value: NA,
					'CURRENCY'			=> $currency,
					'PURCHASECURRENCY'	=> $currency,
					'AMOUNT'			=> $amount,
					'PURCHASEAMOUNT'	=> $amount,
					'TRANSIDMERCHANT'	=> $transactionId,
					'SHAREDKEY'			=> $sharedKey,
					'WORDS'				=> $generateWord,
					'REQUESTDATETIME'	=> $requestDateTime,
					'SESSIONID'			=> $sessionId,
					'PAYMENTCHANNEL'	=> $paymentChanel,
					'EMAIL'				=> $email,
					'NAME'				=> $name,
					'ADDRESS'			=> $address,
					'HOMEPHONE'			=> $homephone,
					'MOBILEPHONE'		=> $homephone,
					'ZIPCODE'			=> $zipcode,
					'WORKPHONE'			=> $homephone
	       		]; 
	            $paymentDoku = new PaymentDokuModel();
				$data = [
					'co_transIdMerchat'=> $transactionId,
					'sessionId'=> $sessionId,
					'word'=>$generateWord,
					'created_at'=>date('Y-m-d H:i:s')
				];
				$dokuData = $paymentDoku->insert($data);
				echo view('doku/invoice_payment',$parameters);
	        } else {
				$data['message'] = 'Payment Link Expired';
	        	echo view('doku/failed_payment', $data);
	        }
   		}

   		
   	}
    private function randomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
    public function redirect(){
		return redirect()->to('http://voxnet.id/');
	}
	public function identify(){
		echo "<pre>";
			$paymentDoku = new PaymentDokuModel();
			$dokuData = $paymentDoku->findAll();
			print_r($dokuData);
	}

	function findArray($collection,$value){
		return array_values(array_filter($collection, function($var) use ($value) { return preg_match("/\b$value\b/i", $var); }));
	}

	public function notify(){
		$dumpResult = file_get_contents('php://input');
		$paymentDoku = new PaymentDokuModel();

		try {
			// Parsing Data for Doku Payment Find TransactionID
			$exploreResultDoku = explode("&",$dumpResult);
			$searchword = 'TRANSIDMERCHANT';
			$findMatched = $this->findArray($exploreResultDoku,$searchword);
			$xplodMatched = explode("=",$findMatched[0]);
			$transactionID = $xplodMatched[1];

			// Get Result Transasction
			$findResult = "RESULTMSG";
			$resultTransaction = $this->findArray($exploreResultDoku,$findResult);
			$xplodeResult = explode("=",$resultTransaction[0]);
			$statusResult = $xplodeResult[1];

			$findResultS = "SESSIONID";
			$resultSession = $this->findArray($exploreResultDoku,$findResultS);
			$xplodeResultSession = explode("=",$resultSession[0]);
			$sessionResult = $xplodeResultSession[1];

			$randomPassword = substr($this->randomString(10), 0,6);

			$paymentDoku->where('co_transIdMerchat',$transactionID)
	        			->where('sessionId',$sessionResult)
	        			->set('status',$statusResult)
	        			->set('doku_response',$dumpResult)
	        			->set('updated_at',date('Y-m-d H:i:s'))
						->update();

			if ($statusResult == 'SUCCESS') {
				// Temporarly Remove $randomPassword
				// email invoice + credential should be in the end after all activation is success
				// however, did number can be assigned and that's chance of failed activation
				// so, invoice without credential will be send first before activation
				$this->emailInvoice($transactionID);

				$this->createPbxAccount($transactionID,$randomPassword);
			}

			echo "ACTIVATION SUCCESS";
			
	        
		} catch (Exception $e) {
			$data = [
				'doku_response'=>$dumpResult
			];
			$paymentDoku->insert($data);	
			$this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'error paymentdoku notify');
			echo "SOMETHING WRONG";
		}
		
		
    }
    
	
	function createPbxAccount($transactionID = '[NOT AVAILABLE]',$randomPassword = '[NOT AVAILABLE]'){
		$customerOrderModel = new CustomerOrderModel();
        $customerOrderResult = $customerOrderModel->where(['co_num_transidmerchant' => $transactionID])->first();
        if ($customerOrderResult) {
        	$customerCompaniesModel = new CustomerCompaniesModel();
	        $customerCompaniesModel->join('customer_address','customer_companies.cc_id=customer_address.cc_id');
	        $customerCompaniesModel->join('customer_user','customer_companies.cc_id=customer_user.cc_id');
	        $customerDetail = $customerCompaniesModel->where(['customer_companies.cc_id' => $customerOrderResult['cc_id']])->first();
	        
			$json = array(
				'companyName'=> $customerDetail['cc_name'],
				'cu_name_email'=> $customerDetail['cu_name_email'],
				'password'=> $randomPassword,
				'transactionId'=>$transactionID,
				'address'=>  $customerDetail['ca_address_1'].' '.$customerDetail['ca_address_2'],
				'ca_zipcode'=> $customerDetail['ca_zipcode'],
				'city'=> $customerDetail['ca_lp_id'],
				'phoneNumber'=> $customerDetail['cc_phone'],
				'mobileNumber'=> $customerDetail['cu_name_mobile'],
				'activated_by'=>'BucketAPI',
			);

			$this->Voipswitch->create_pbx_account_pd($json, $transactionID);
			$this->addBalance($transactionID);
			
			
        } else {
			$this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'error paymentdoku createPBXAccount');
        	echo 'Order with co_id or transactionId is not Exist!';
        }

	}
	function addBalance($transactionID = '[NOT AVAILABLE]'){
		$customerOrderModel = new CustomerOrderModel();
        $customerOrderResult = $customerOrderModel->where(['co_num_transidmerchant' => $transactionID])->first();

        $loginParams = $customerOrderResult['login'];

        $customerAddon = new CustomerOrderAddonModel();
		$customerAddon->join('product_addon pa','pa.pa_id=customer_order_addon.pa_id');
		$customerAddon->join('product_addon_matrix pam','pam.pam_id=customer_order_addon.pam_id');
        $pDetail = $customerAddon->where(['co_id'=>$customerOrderResult['co_id']])->findAll();
        $taddonValueTCOut = array();
        $taddonValueTLOut = array();
        $taddonValueSAOut = array();
        $addonValueTC = array();
        $addonValueTL = array();
        $addonValueSA = array();
        for ($i=0; $i <count($pDetail) ; $i++) { 
        	$addonItemName = $pDetail[$i]['pa_name'];
        	$addonValue = $pDetail[$i]['coa_qty_total'];
        		
        	// Get From talk credit
        	if ($addonItemName == 'Add-On Talk-Credit') {
        		$addonValueTC = $addonValue; 
        	}
        	else {
        		$addonValueTC = 0;
        	}
        	// Get From Trunk Lines
        	if ($addonItemName == 'Add-On Trunk-Lines') {
        		$addonValueTL = $addonValue;
        	} else {
        		$addonValueTL = 0;
        	}
        	// // Get From Add-On Sub-Accounts
        	if ($addonItemName == 'Add-On Sub-Accounts') {
        		$addonValueSA = $addonValue;
        	} else {
        		$addonValueSA = 0;
        	}

        	$taddonValueTC[] = $addonValueTC;
        	$taddonValueTL[] = $addonValueTL;
        	$taddonValueSA[] = $addonValueSA;

        	array_push($taddonValueTCOut, array_sum($taddonValueTC));
	        array_push($taddonValueTLOut, array_sum($taddonValueTL));
	        array_push($taddonValueSAOut, array_sum($taddonValueSA));
        	
        }
        
        $MoneyValue = $customerOrderResult['co_name_talkcredit']+end($taddonValueTCOut);
        $MaxClient = $customerOrderResult['co_name_maxsub']+end($taddonValueSAOut);
        $TrunkLine = $customerOrderResult['co_name_trunknum']+end($taddonValueTLOut);

		$json = array(
			'login'=>$loginParams,
			'money'=>$MoneyValue,
			'description'=>"Add balance from bucketapi",
		);
		
		$this->Voipswitch->add_balance_pd($json, $transactionID);
		
		$this->updateSubTrunk($loginParams,$MaxClient,$TrunkLine, $transactionID);

	}
	function updateSubTrunk($login,$maxsub,$maxtrunk, $transactionID = '[NOT AVAILABLE]'){
		$json = array(
			'login'=>$login,
			'maxsub'=>$maxsub,
			'trunkline'=>$maxtrunk,
		);
		$this->Voipswitch->update_subtrunk_pd($json, $transactionID);	

		// $this->assign_did_number($loginParams,$transactionID);

		$this->emailNotification($transactionID, 'failed_did', 'Request Assign DID Number');
	}
	
	function assign_did_number($login,$transactionID){
		$customerOrderModel = new CustomerOrderModel();
        $customerOrderResult = $customerOrderModel->where(['co_num_transidmerchant' => $transactionID])->first();

        $customerOrderTelephone = new CustomerOrderTelephoneModel();
		$customerOrderTelephone->join('customer_order co','co.co_id=customer_order_telephone.co_id');
        $customerOrderTelp = $customerOrderTelephone->where(['customer_order_telephone.co_id'=>$customerOrderResult['co_id']])->findAll();

        for ($i=0; $i < count($customerOrderTelp) ; $i++) { 
        	$json = array(
				'login'=>$login,
				'did_number'=>$customerOrderTelp[$i]['cot_telephone'],
				'disable_verification'=> true
			);
			$this->Voipswitch->assign_did_number_pd($json);
        }
		
	}

	function emailInvoice($transactionID = '[NOT AVAILABLE]', $randomPassword = '[NOT AVAILABLE]'){
		$customerOrderModel = new CustomerOrderModel();
        $customerOrderResult = $customerOrderModel->where(['co_num_transidmerchant' => $transactionID])->first();

        $customerCompaniesModel = new CustomerCompaniesModel();
        $customerCompaniesModel->join('customer_address','customer_companies.cc_id=customer_address.cc_id');
        $customerCompaniesModel->join('customer_user','customer_companies.cc_id=customer_user.cc_id');
        $customerDetail = $customerCompaniesModel->where(['customer_companies.cc_id' => $customerOrderResult['cc_id']])->first();

        $url = 'https://bucketapi.voxnet.id/public/mailersmtp/send_invoice';

		// Send Invoice Email to Customer
        $data = array(
        	"co_id" => $customerOrderResult['co_id'],
		    "destination" => $customerDetail['cu_name_email'],
		    'template' => 'invoice',
			'pending_activation' => true,
		    // "sip_login"=>$customerOrderResult['login'],
		    // "username" => $customerDetail['cu_name_email'],
		    // "password" => $randomPassword
        );
         
        $this->curlModel->content_gc($url,$data,"POST");

	}

	function emailNotification($transactionID = '[NOT AVAILABLE]', $content = 'failed_activation', $subject = null, $error_msg = null){
		$customerOrderModel = new CustomerOrderModel();
        $customerOrderDetail = $customerOrderModel->where(['co_num_transidmerchant' => $transactionID])->first();

		// Send Invoice Email to Sales
		$url = 'https://bucketapi.voxnet.id/public/mailersmtp/send_sales_notification';
		$data = array(
        	"co_id" => $customerOrderDetail['co_id'],
		    "destination" => 'billing@voxnet.id',
			"content" => $content,
			"subject" => $subject,
			"error_msg" => $error_msg,
        );

        $this->curlModel->content_gc($url,$data,"POST");
	}

}
