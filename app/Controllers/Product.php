<?php

namespace App\Controllers;
use App\Models\ProductModel;
use App\Models\ProductAddonModel;
use App\Models\ProductAddonItemModel;
use App\Models\ProductAddonMatrixModel;
use App\Models\ProductCityServedModel;
use App\Models\ProductCreditBonusModel;
use App\Models\ProductDIDNumberModel;
use App\Models\CustomerOrderTelephoneModel;
use CodeIgniter\RESTful\ResourceController;

class Product extends ResourceController
{

    public function __construct()
	{
		$this -> product = new ProductModel();
        $this -> productAddon = new ProductAddonModel();
        $this -> productAddonItem = new ProductAddonItemModel();
        $this -> productAddonMatrix = new ProductAddonMatrixModel();
        $this -> productCityServed = new ProductCityServedModel();
        $this -> productCreditBonus = new ProductCreditBonusModel();
        $this -> productDIDNumber = new ProductDIDNumberModel();

        $this -> customerOrderTelephone = new CustomerOrderTelephoneModel();
	}


    public function index()
	{
        $products = $this->product->where(['p_id_par' => 0])->findAll();
      
        if ($products) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Product Found",
                "data" => $products,
            ];
            return $this->respond($response, 200);
        } else {
            return $this->failNotFound('No Product Found');
        }
	}

    public function package()
    {
        $packages = $this->product->where(['p_id_par !=' => 0])->findAll();
        
        if ($packages) {

            foreach ($packages as $package)
            {
                // Filter Packages, return only column that has value
                $packages_filtered[] = (object) array_filter((array) $package, function ($val) {
                    return !is_null($val);
                });
            }

            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Product Packages Found",
                "data" => $packages_filtered,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No Package Found');
        }
    }

    public function addon($p_id = null)
    {
        $addons = $this->productAddon->findAll();

        $addonMatrixs = $this->productAddonMatrix->findAll();

        $addonItems = $this->productAddonItem->findAll();
                             
        if ($addons && $addonMatrixs && $addonItems) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Product Addon Found",
                "data" => [
                    'productAddons' => $addons,
                    'productAddonItems' => $addonItems,
                    'productAddonMatrixs' => $addonMatrixs,
                ]
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No Addon Found');
        }
    }

    public function cityServed($pdn_area = null)
    {
        $cityServed = $this->productCityServed->findAll();

        if($cityServed)
        {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Product City Served Found",
                "data" => $cityServed,
            ];
            return $this->respond($response);
        }

        else 
        {
            return $this->failNotFound('No Product City Served Found');
        }
    }

    public function creditBonus()
    {
        $creditBonus = $this->productCreditBonus->findAll();

        if($creditBonus)
        {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Product City Served Found",
                "data" => $creditBonus,
            ];
            return $this->respond($response);
        }

        else 
        {
            return $this->failNotFound('No Product Bonus Served Found');
        }
    }

    public function DIDNumber($pdn_area = null)
    {
        try {
            // If pdn area is not provided, return all available pdn_area from DB
            if(!$pdn_area) 
            {   
                $pdn_areas = [];
                $productDIDNumbers = [];
                
                foreach ( $this->productDIDNumber->select('pdn_area, pdn_area_code')->findAll() as $pdn )
                {
                    $pdn_area = $pdn['pdn_area'];
                    
                    if(!in_array($pdn_area, $pdn_areas))
                    {
                        $pdn_areas[] = $pdn_area;
                        $productDIDNumbers[] = $pdn;
                    }
                }
    
                $response = [
                    'status' => 200,
                    'error' => null,
                    'messages' => "Product DID Number's Areas Found",
                    "data" => $productDIDNumbers,
                ];
                return $this->respond($response);
            }

            // if pdn_area is provided, return all available did numbers from db
            else {
                $productDIDNumbers = [];
            
                foreach( 
                    $this->productDIDNumber
                        ->where('pdn_area', $pdn_area)
                        ->where('pdn_status', 'available')
                        ->select('pdn_id, pdn_did_number, pdn_area, pdn_area_code, pdn_status')
                        ->findAll() as $pdn
                )
                {
                    $ordered_pdn = $this->customerOrderTelephone
                                    ->where(['cot_telephone' => $pdn['pdn_did_number']])->first();

                    if(!isset($ordered_pdn))
                    {
                        $productDIDNumbers[] = $pdn;
                    }
                }

                $response = [
                    'status' => 200,
                    'error' => null,
                    'messages' => "Product DID Number at ".$pdn_area." Found",
                    "data" => $productDIDNumbers,
                ];
                return $this->respond($response);
            }

        } catch (\Throwable $th) {
            return $this->failNotFound('No Product DID Number Found');
        }

    }

}
