<?php

namespace App\Controllers;
use App\Models\SubTrunkModel;
use CodeIgniter\RESTful\ResourceController;

class SubTrunk extends ResourceController
{

	public function show($login = null)
	{
		$model = new SubTrunkModel();
        $data = $model->getWhere(['login' => $login])->getResult();
        if ($data) {
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Max Sub Account and Trunk Line Found",
                "data" => $data,
            ];
            return $this->respond($response);
        } else {
            return $this->failNotFound('No Max Sub Account and Trunk Line Found with login ' . $login);
        }
	}

	public function update($id = null)
	{
		$model = new SubTrunkModel();
        $data = $model->where(['id' => $id])->first();
        $data = [
            'MaxClients' => $this->request->getVar('MaxClients'),
            'type2' => $this->request->getVar('type2'),
        ];

        $model->update($id, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => "Max Sub Account and Trunk Line Added"
        ];
        return $this->respond($response);
	}


}
