<?php

namespace App\Controllers;
use App\Models\UserModel;
use App\Models\CustomerUserModel;
use App\Models\CustomerCompaniesModel;
use CodeIgniter\RESTful\ResourceController;

class Unique extends ResourceController
{
	public function __construct()
	{
    $this -> user = new UserModel();
    $this -> customerUser = new CustomerUserModel();
    $this -> customerCompanies = new CustomerCompaniesModel();
  }

  public function customerCompanies($var_name='cc_', $var_value = null)
  { 
    $data = $this->customerCompanies->where([$var_name => $var_value])->first();
        
    $is_unique = ($data) ? (false) : (true);

    $response = [
      'status' => 200,
      'error' => null,
      'data' => [
        'is_unique' => $is_unique,
      ]
    ];

    return $this->respond($response);
  }

  public function customerUser($var_name='cu_', $var_value = null)
  {
    $data = $this->customerUser->where([$var_name => $var_value])->first();
      
    $is_unique = ($data) ? (false) : (true);

    $response = [
      'status' => 200,
      'error' => null,
      'data' => [
        'is_unique' => $is_unique,
      ]
    ];

    return $this->respond($response);
  }

  public function user($var_name='username', $var_value = null)
  {
    $data = $this->user->where(['username' => $var_value])->first();
      
    $is_unique = ($data) ? (false) : (true);

    $response = [
      'status' => 200,
      'error' => null,
      'data' => [
        'is_unique' => $is_unique,
      ]
    ];

    return $this->respond($response);
  }
  
}