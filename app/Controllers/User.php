<?php

namespace App\Controllers;
use App\Models\UserModel;
use App\Models\CustomerUserModel;
use App\Models\CustomerCompaniesModel;
use App\Models\CustomerAddressModel;
use App\Models\CustomerOrderModel;
use App\Models\CustomerOrderAddonModel;
use App\Models\CustomerOrderTelephoneModel;
use App\Models\ProductModel;
use App\Models\MasterModel;
use App\Models\CurlModel;
use CodeIgniter\RESTful\ResourceController;

class User extends ResourceController
{
	public function __construct()
	{
		$this -> user = new UserModel();
		$this -> customerUser = new CustomerUserModel();
		$this -> customerCompanies = new CustomerCompaniesModel();
		$this -> customerAddress = new CustomerAddressModel();
		$this -> customerOrder = new CustomerOrderModel();
		$this -> customerOrderAddon = new CustomerOrderAddonModel();
		$this -> customerOrderTelephone = new CustomerOrderTelephoneModel();
		$this -> product = new ProductModel();
		$this -> master = new MasterModel();
		$this -> curl = new CurlModel();

		date_default_timezone_set('Asia/Jakarta');
	}

	// public function date()
	// {
	// 	$correct_date = date('Y-m-d H:i:s');
		
	// 	$response = [
	// 		'status' => 200,
	// 		'error' => null,
	// 		'date'=>$correct_date
	// 	];

	// 	return $this->respond($response);
	// }

	// public function tomd5($password = null)
	// {
	// 	$hashedPassword = md5($password);
		
	// 	$response = [
	// 		'md5' => $hashedPassword,
	// 	];

	// 	return $this->respond($response);
	// }

	public function register()
	{
		try {
        	# INSERT USER DATA
			$user = json_decode($this->request->getVar('userParams'));
			if($user)
			{	
				$randomPassword = rand(100000,999999);
		    	$encryptedPassword = md5($randomPassword);
		    	$defaulPermission = '4';

				$user = [
					'nama' => $user -> nama,
					'username'=> $user -> username,
					'password'=> $encryptedPassword,
					'permission'=> $defaulPermission,
				];

				$this->user->insert($user);	
			}

			# INSERT CUSTOMER COMPANIES DATA
			$customerCompanies = json_decode($this->request->getVar('customerCompaniesParams'));
			if($customerCompanies)
			{
				$cc_id = $this->customerCompanies->insert($customerCompanies);	
			}

			# INSERT CUSTOMER USER DATA
			$customerUser = json_decode($this->request->getVar('customerUserParams'));
			if($customerUser) 
			{
				$customerUser -> cc_id = $cc_id;
				$this -> customerUser -> insert($customerUser);	
			}

			// # INSERT CUSTOMER ADDRESS DATA
			$customerAddress = $this->request->getVar('customerAddressParams');
			foreach($customerAddress as $element) 
			{
				$element = json_decode($element);
				$element -> cc_id = $cc_id;
				$this -> customerAddress -> insert($element);
			}

			# INSERT CUSTOMER ORDER DATA
			$customerOrder = json_decode($this->request->getVar('customerOrderParams'));
			$customerOrder -> cc_id = $cc_id;
        	$customerOrder -> co_num_ip = 'PI';
        	$customerOrder -> co_num_transdate = date('Ymd');
        	$customerOrder -> co_num_transcode = 'TRX';
        	$customerOrder -> co_num_transidmerchant = $this -> master -> alphanumeric();
        	$customerOrder -> co_status = 1;
        	$customerOrder -> co_createdate = date('Y-m-d H:i:s');
			// 'co_name'		=> $data['product']['p_name'],
			// 'co_name_sub'	=> $data['product_sub']['p_name'],

			$co_id = $this -> customerOrder -> insert($customerOrder);

			# INSERT CUSTOMER ORDER ADDON DATA
			$customerOrderAddon = $this->request->getVar('customerOrderAddonParams');

			if($customerOrderAddon)
			{
				foreach($customerOrderAddon as $element) 
				{
					$element = json_decode($element);
					$element -> co_id = $co_id;
					$this -> customerOrderAddon -> insert($element);
				}
			}

			# INSERT CUSTOMER ORDER TELEPHONE DATA
			$customerOrderTelephone = $this->request->getVar('customerOrderTelephoneParams');
			if($customerOrderTelephone)
			{
				foreach($customerOrderTelephone as $element) 
				{
					$element = json_decode($element);
					$element -> co_id = $co_id;
					$this -> customerOrderTelephone -> insert($element);
				}
			}

			$response = [
				'status' => 200,
				'error' => null,
				'messages' => "New Subscription Registered",
				'data' => [
					'co_id' => $co_id,
				]
			];

	    	return $this->respondCreated($response);

		} catch (\Throwable $th) {
			$response = [
				'status' => 400,
				'error' => $th,
				'messages' => "Something Wrong",
				"data" => [],
			];
			return $this->respond($response);
		}
		
	}

	public function extend()
	{
		
		try {
			$cc_id = $this->request->getVar('cc_id'); 
		
			# INSERT CUSTOMER ORDER DATA
			$customerOrder = json_decode($this->request->getVar('customerOrderParams'));
			$customerOrder -> cc_id = $cc_id;
			$customerOrder -> co_num_ip = 'PI';
			$customerOrder -> co_num_transdate = date('Ymd');
			$customerOrder -> co_num_transcode = 'TRX';
			$customerOrder -> co_num_transidmerchant = $this -> master -> alphanumeric();
			$customerOrder -> co_status = 1;
			$customerOrder -> co_createdate = date('Y-m-d H:i:s');
			$co_id = $this -> customerOrder -> insert($customerOrder);
	
	
			# INSERT CUSTOMER ORDER ADDON DATA
			$customerOrderAddon = $this->request->getVar('customerOrderAddonParams');
			if($customerOrderAddon)
			{
				foreach($customerOrderAddon as $element) 
				{
					$element = json_decode($element);
					$element -> co_id = $co_id;
					$this -> customerOrderAddon -> insert($element);
				}
			}
	
			# INSERT CUSTOMER ORDER TELEPHONE DATA
			$customerOrderTelephone = $this->request->getVar('customerOrderTelephoneParams');
			if($customerOrderTelephone)
			{
				foreach($customerOrderTelephone as $element) 
				{
					$element = json_decode($element);
					$element -> co_id = $co_id;
					$this -> customerOrderTelephone -> insert($element);
				}
			}
	
			$response = [
				'status' => 200,
				'error' => null,
				'messages' => "Extend Subscription Registered",
				'data' => [
					'co_id' => $co_id
				],
			];
	
			return $this->respondCreated($response);

		} catch (\Throwable $th) {
			$response = [
				'status' => 400,
				'error' => $th,
				'messages' => "Something Wrong",
				"data" => [],
			];
			return $this->respond($response);
		}
		
	}

	public function addon()
	{
		try {
			$co_id = $this->request->getVar('co_id'); 

			# INSERT CUSTOMER ORDER ADDON DATA
			$customerOrderAddon = $this->request->getVar('customerOrderAddonParams');
			if($customerOrderAddon)
			{
				foreach($customerOrderAddon as $element) 
				{
					$element = json_decode($element);
					$element -> co_id = $co_id;
					$this -> customerOrderAddon -> insert($element);
				}
			}
	
			# INSERT CUSTOMER ORDER TELEPHONE DATA
			$customerOrderTelephone = $this->request->getVar('customerOrderTelephoneParams');
			if($customerOrderTelephone)
			{
				foreach($customerOrderTelephone as $element) 
				{
					$element = json_decode($element);
					$element -> co_id = $co_id;
					$this -> customerOrderTelephone -> insert($element);
				}
			}
	
			$response = [
				'status' => 200,
				'error' => null,
				'messages' => "Subscription Addon Registered",
			];
	
			return $this->respondCreated($response);
		
		} catch (\Throwable $th) {
			$response = [
				'status' => 400,
				'error' => $th,
				'messages' => "Something Wrong",
				"data" => [],
			];
			return $this->respond($response);
		}

	}

	public function login()
	{

		$email = $this->request->getVar('email');
		$password = $this->request->getVar('password');

		$userData = $this->user
	    ->where(['username' => $email])
			->where(['password' => md5($password)])
			->orWhere(['password' => $password])
			->select('username')
		  ->first();

		if(empty($userData))
		{
			$response = [
				'messages' => 'Incorrect Email or Password'
		    ];
			return $this->respond($response, 400);
		}

		else
		{
			$customerUserData = $this->customerUser
			  ->where(['cu_name_email' => $email])	
				->select("cc_id")
        ->select("cu_name_first")
        ->select("cu_name_last")
        ->select("cu_name_dsp")
        ->select("cu_name_email")
        ->select("cu_name_mobile")
				->first();

			$cc_id = $customerUserData["cc_id"];

			$customerCompaniesData = $this->customerCompanies
				->where(['cc_id' => $cc_id])
				->select("cc_id")
				->select("cc_name")
				->select("cc_npwp")
				->select("cc_phone")
				->select("cc_web")
				->first();

			$customerAddressData = $this->customerAddress->where(['cc_id' => $cc_id])->findAll();	

			foreach($this->customerOrder->where(['cc_id' => $cc_id])->findAll() as $order)
			{
				$order['customerOrderPackage'] = [
					'p_id' => $order['p_id'],
					'p_name' => $order['co_name'],
					'p_name_maxsub' => $order['co_name_maxsub'],
					'p_name_maxtalkcredit' => $order['co_name_maxtalkcredit'],
					'p_name_monthlysubscribe' => $order['co_name_monthlysubscribe'],
					'p_name_talkcredit' => $order['co_name_talkcredit'],
					'p_name_telnumber' => $order['co_name_telnumber'],
					'p_name_trunknum' => $order['co_name_trunknum'],
				];
				$customerOrderProduct = 
				  $this->product->where('p_id', $order['p_id'])->select('p_id_par')->first();
				$order['customerOrderProduct'] = $customerOrderProduct['p_id_par'];
		
				$customerOrderData[] = $order;
				$co_ids[] = $order['co_id'];
				}
			
			$customerOrderAddonData = $this->customerOrderAddon->whereIn('co_id', $co_ids)->findAll();

			$customerOrderTelephoneData = $this->customerOrderTelephone->whereIn('co_id', $co_ids)->findAll();

			// foreach($this->customerOrderTelephone->whereIn('co_id', $co_ids)->findAll() as $cot)
			// {
			//   $cot['cot_telephone'] = explode(',', $cot['cot_telephone']);
			// 	$customerOrderTelephoneData[] = $cot;
			// }

			$response = [
				'status' => 201,
				'error'  => false,
				'data'   => [
					'auth_token' => md5($password),
					'customerCompanies' => $customerCompaniesData,
					'customerAddress' => $customerAddressData,
					'customerUser' => $customerUserData,
					'customerOrder' => $customerOrderData,
					'customerOrderAddon' => $customerOrderAddonData,
					'customerOrderTelephone' => $customerOrderTelephoneData
				],
			];
		  return $this->respond($response, 201);
		}
	}

	public function resend_credential($email)
	{
			// $data['customer_order_detail']	= $this->master->get_row_param('customer_order','co_id',$co_id);
			// $showAccUrl = 'https://bucketapi.voxnet.id/public/SubTrunk/Show/'.$data['customer_order_detail']['login'];
			// $data['passwordRespon'] = $this->curl->get_gc($showAccUrl);

			//Kirim notifikasi email aktivasi aku ke user
			$credMailUrl = 'https://bucketapi.voxnet.id/public/mailersmtp';
			$credMailJson = array(
				'destination'	=> $email,
				'template'	=> 'credential',
				'sip_login'	=> $email,
				'username' => $email,
				'password'	=> 'HAHAHHAHAHHA'
			);
			$data['credMailRespon'] = $this->curl->content_gc($credMailUrl,$credMailJson,'POST');
	
			return $this->respond(['message' => 'email sent!']);
			// $data = [
			// 	'sip_login' => 'login_mail.com',
			// 	'username' => 'username_user_table',
			// 	'password' => 'loginbro',
			// 	'template' => 'credential',
			// ];
			// $data['doc_title'] = 'Invoice';

			// echo view('mailer/mailer_invoice', $data);
		}
}