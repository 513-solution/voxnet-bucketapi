<?php

namespace App\Controllers;

// Curl and local DB Utility Model
use App\Models\MasterModel;
use App\Models\CurlModel;

// local DB Tables Model
use App\Models\UserModel;
use App\Models\CustomerOrderModel;
use App\Models\CustomerOrderAddonModel;
use App\Models\CustomerOrderTelephoneModel;
use App\Models\ProductDIDNumberModel;

// Voipswitch DB Model
use App\Models\SubTrunkModel;
use App\Models\DidnumberModel;
use App\Models\GetiduserModel;
use App\Models\IbpbxModel;
use App\Models\IbobgetModel;
use App\Models\ObgetModel;


use CodeIgniter\RESTful\ResourceController;

class Voipswitch extends ResourceController
{

    public function __construct()
	{
        $this -> master = new MasterModel();
        $this -> curl = new CurlModel();
        
        $this -> user = new UserModel();
        $this -> customerOrder = new CustomerOrderModel();
        $this -> customerOrderAddon = new CustomerOrderAddonModel();
        $this -> customerOrderTelephone = new CustomerOrderTelephoneModel();
        $this -> productDIDNumber = new ProductDIDNumberModel();

        $this -> subtrunk = new SubTrunkModel();

        // Get id and add did number
        $this -> getIdUser = new GetiduserModel(); 
        $this -> didNumber = new DidNumberModel();

        // Get id and add inbound
        $this -> ibobGet = new ibobGetModel();
        $this -> ibPbx = new IbpbxModel();
        
        // Get id and add outbound
        $this -> obGet = new ObgetModel();

        $this -> userName = getenv('voipswitchUserName');
        $this -> userPassword = getenv('voipswitchUserPassword');
        $this -> passwordHash = sha1($this -> userPassword);
        $this -> webAPIBaseURL = getenv('voipswitchWebAPIBaseURL');

        date_default_timezone_set('Asia/Jakarta');
    }

    public function assign_did_number()
    {
        
        $login = $this->request->getVar('login');
        $did_number = $this->request->getVar('did_number');
        $disable_verification = $this->request->getVar('disable_verification');
        
        // Verify Required Params
        if(!$login || !$did_number)
        {
            return $this -> fail('login and did_number are required!');
        }

        // Verify if login and ordered did_number on local_db
        if(!$disable_verification)
        {
             // Verify if did_number exist and available
            $product_did_number = $this -> productDIDNumber 
                -> where(['pdn_did_number' => $did_number])
                -> where(['pdn_status' => 'available'])
                -> orWhere(['pdn_status' => 'cancelled']) 
                -> first();

            if(!isset($product_did_number))
            {
                return $this->fail('DID Number Not found or already Assigned!');
            }

            // Verify if the did_number ordered by login
            $customer_order = $this->customerOrder->where(['login'=>$login])->first();

            if(!isset($customer_order))
            {
                return $this->failNotFound('Customer order Not found!');
            }

            // Verify if account is activated
            if($customer_order['co_status'] != 2)
            {
                return $this->fail('Needs Account Activation!');
            }

            $co_id = $customer_order['co_id'];
            $customer_order_telephone = $this -> customerOrderTelephone
                          -> where(['cot_telephone' => $did_number, 'co_id' => $co_id])
                          -> first();


            if(!isset($customer_order_telephone))
            {
                return $this->fail('DID Number not ordered by '.$login.'!');
            }
        } else {
            $product_did_number = $this -> productDIDNumber 
                -> where(['pdn_did_number' => $did_number])
                -> first();

            if(!isset($product_did_number))
            {
                return $this->fail('DID Number Not found or already Assigned!');
            }
        }
   
        // Get user data that contain id_client and id_company
        $data_id_client_did = $this -> getIdUser->where(['login' => $login])->first();
        $data_id_company_ib = $this -> ibobGet->where(['login' => $login])->first();
        $data_id_client_ob = $this -> obGet->where(['login' => $login])->first();
    
        if (
            !isset($data_id_client_did['id_client']) 
            || !isset($data_id_company_ib['id_company'])
            || !isset($data_id_client_ob['id_client'])
        ) { 
            return $this->failNotFound('No IDs Found with login ' . $login);
        } else {


        // Insert DID Number to voipswitch DB

            // Get id_client for add did number
            $id_client_did = $data_id_client_did['id_client'];
            $dataDid = [
                'did_number' => $did_number,
                'area_name' => $product_did_number["pdn_area"],
                'area_code' => $product_did_number["pdn_area_code"],  
                'country_id' => 62,
                'client_id' => $id_client_did,
	            'client_type' => 32,
            ];

            $this -> didNumber -> insert($dataDid);

        // Add Inbound Dialplan

            // get id_company for add inbound
            $id_company_ib = $data_id_company_ib['id_company'];
            $inbound_data = [
                'telephone_number' => $did_number,
                'tech_prefix' => 'PBX;DN:'.$id_company_ib.'#',
                'id_route' => $id_company_ib,

                'priority' => 0,
                'route_type' => 4,
                'call_type' => 0,
                'type' => 4,
                'balance_share' => 100,
                'call_limit' => 1,
                'id_dial_plan' => 1,     
            ];

            $this->ibPbx->insert($inbound_data);

        // Update Oubound dialroute

             // get id_client for add outbound
             $id_client_ob = $data_id_client_ob['id_client'];
             $tect_prefix = 'CP:!'.$did_number.';DP:int010->010 OR int0->62 OR 0->62 OR 88881->BLOCK OR int-> OR +-> OR onnet->;TP:int010->010 OR int0->62 OR 0->62 OR int-> OR +-> OR onnet->62';

             $outbound_data = $this->obGet->where(['id_client' => $id_client_ob])->first();
             $outbound_data = [
                'tech_prefix' => $tect_prefix,
            ];

            $this->obGet->update($id_client_ob, $outbound_data);

            // Update DID Number status on product_did_number
            $this -> productDIDNumber
                  -> set('pdn_status', 'assigned')
                  -> where(['pdn_did_number' => $did_number])
                  -> update();

            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Did Number ".$did_number." Added to login ".$login,
            ];
      
            return $this->respondCreated($response);
        }

        return $this->fail('Something wrong when assign did number', 500);

    }
    public function assign_did_number_pd($data)
    {
        
        $login = $data['login'];
        $did_number = $data['did_number'];
        $disable_verification = $data['disable_verification'];
        
        // Verify Required Params
        if(!$login || !$did_number)
        {
            echo json_encode('login and did_number are required!');
            die();
        }

        // Verify if login and ordered did_number on local_db
        if(!$disable_verification)
        {
             // Verify if did_number exist and available
            $product_did_number = $this -> productDIDNumber 
                -> where(['pdn_did_number' => $did_number])
                -> where(['pdn_status' => 'available'])
                -> orWhere(['pdn_status' => 'cancelled']) 
                -> first();

            if(!isset($product_did_number))
            {
                echo json_encode('DID Number Not found or already Assigned!');
                die();
            }

            // Verify if the did_number ordered by login
            $customer_order = $this->customerOrder->where(['login'=>$login])->first();

            if(!isset($customer_order))
            {
                echo json_encode('Customer order Not found!');
                die();
            }

            // Verify if account is activated
            if($customer_order['co_status'] != 2)
            {
                echo json_encode('Needs Account Activation!');
                die();
            }

            $co_id = $customer_order['co_id'];
            $customer_order_telephone = $this -> customerOrderTelephone
                          -> where(['cot_telephone' => $did_number, 'co_id' => $co_id])
                          -> first();


            if(!isset($customer_order_telephone))
            {
                echo json_encode('DID Number not ordered by '.$login.'!');
                die();
            }
        } else {
            $product_did_number = $this -> productDIDNumber 
                -> where(['pdn_did_number' => $did_number])
                -> first();

            if(!isset($product_did_number))
            {
                echo json_encode('DID Number Not found or already Assigned!');
                die();
            }
        }
   
        // Get user data that contain id_client and id_company
        $data_id_client_did = $this -> getIdUser->where(['login' => $login])->first();
        $data_id_company_ib = $this -> ibobGet->where(['login' => $login])->first();
        $data_id_client_ob = $this -> obGet->where(['login' => $login])->first();
    
        if (
            !isset($data_id_client_did['id_client']) 
            || !isset($data_id_company_ib['id_company'])
            || !isset($data_id_client_ob['id_client'])
        ) { 
            echo json_encode('No IDs Found with login ' . $login);
            die();
        } else {


        // Insert DID Number to voipswitch DB

            // Get id_client for add did number
            $id_client_did = $data_id_client_did['id_client'];
            $dataDid = [
                'did_number' => $did_number,
                'area_name' => $product_did_number["pdn_area"],
                'area_code' => $product_did_number["pdn_area_code"],  
                'country_id' => 62,
                'client_id' => $id_client_did,
                'client_type' => 32,
            ];

            $this -> didNumber -> insert($dataDid);

        // Add Inbound Dialplan

            // get id_company for add inbound
            $id_company_ib = $data_id_company_ib['id_company'];
            $inbound_data = [
                'telephone_number' => $did_number,
                'tech_prefix' => 'PBX;DN:'.$id_company_ib.'#',
                'id_route' => $id_company_ib,

                'priority' => 0,
                'route_type' => 4,
                'call_type' => 0,
                'type' => 4,
                'balance_share' => 100,
                'call_limit' => 1,
                'id_dial_plan' => 1,     
            ];

            $this->ibPbx->insert($inbound_data);

        // Update Oubound dialroute

             // get id_client for add outbound
             $id_client_ob = $data_id_client_ob['id_client'];
             $tect_prefix = 'CP:!'.$did_number.';DP:int010->010 OR int0->62 OR 0->62 OR 88881->BLOCK OR int-> OR +-> OR onnet->;TP:int010->010 OR int0->62 OR 0->62 OR int-> OR +-> OR onnet->62';

             $outbound_data = $this->obGet->where(['id_client' => $id_client_ob])->first();
             $outbound_data = [
                'tech_prefix' => $tect_prefix,
            ];

            $this->obGet->update($id_client_ob, $outbound_data);

            // Update DID Number status on product_did_number
            $this -> productDIDNumber
                  -> set('pdn_status', 'assigned')
                  -> where(['pdn_did_number' => $did_number])
                  -> update();

            $response = [
                'status' => 200,
                'error' => null,
                'messages' => "Did Number ".$did_number." Added to login ".$login,
            ];
      
            echo json_encode($response);
        }

        echo 'Something wrong when assign did number';
        die();

    }
    
    public function create_pbx_account()
    {
        $companyName = $this->request->getVar('companyName');
        $cu_name_email = $this->request->getVar('cu_name_email');
        $password = $this->request->getVar('password');

        // Verify if Required params are provided
        if (!$companyName || !$cu_name_email || !$password)
        {
            return $this -> fail('companyName, cu_name_email, and password cannot be empty!');
        }

        // Verify if cu_name_email exist in User table
        $user_data = $this->user->where(['username' => $cu_name_email])->first();
        if(!isset($user_data))
        {
            return $this->fail('username with cu_name_email not exist!');
        }

        // Verify if customer order with given co_id or transactionId is exist
        $customer_order_detail = null;
        $co_id = $this->request->getVar('co_id');
        $transactionId = $this->request->getVar('transactionId');

        if($co_id)
        {
            $customer_order_detail = $this->master->get_row_param('customer_order','co_id', $co_id);
        }

        else if($transactionId)
        {
            $customer_order_detail = $this->master->get_row_param('customer_order','co_num_transidmerchant', $transactionId);
        }

        if(!isset($customer_order_detail))
        {
            return $this->fail('Order with co_id or transactionId is not Exist!');
        }

        // Prepare Json data to be sent to webAPI
        $json = array(
            'companyName'	=> $companyName,
            'login'	=> str_replace('@', '_', $this->request->getVar('cu_name_email')),
            'password'		=> $password,
            'webPassword'	=> $password,
            'address'	=> $this->request->getVar('address'),
            'zipCode'	=> $this->request->getVar('ca_zipcode'),
            'city'	=> $this->request->getVar('city'),
            'taxID'	=> "PPN",
            'eMail'	=> $this->request->getVar('cu_name_email'),
            'phoneNumber'	=> $this->request->getVar('phoneNumber'),
            'mobileNumber'	=> $this->request->getVar('mobileNumber'),
            'tariffId'		=> 111,
            'accountState'	=> 1,
            'generateInvoice'	=> false,
            'sendInvoice'	=> false
        );

        $sendURL = $this->webAPIBaseURL.'/json/syncreply/AdminCreatePBXAccount';

        try {
            $response = $this->curl->content_gc_basic_auth(
                $sendURL,
                $json,
                'POST',
                $this->userName,
                $this->passwordHash
            );

        } catch (\Throwable $th) {
            return $this->fail("Something Wrong When Send WebAPI", 500);;
        }
        
        // try{
            if(isset($response['responseStatus']))
            {
                return $this -> fail($response['responseStatus']);
            }

            else {
                
				 // Update datas to customer_order table

                $co_activatedby = $this->request->getvar('activated_by');

                if(!$co_activatedby)
                {
                    $co_activatedby = 'BucketAPI';
                }

				$data_update_order = array(
					'idClient'	=> $response['idClient'],
					'idCompany'	=> $response['idCompany'],
					'login'		=> $response['login'],
					'cu_name_email'	=> $cu_name_email,
					'co_num_inv'	=> 'INV',
					'co_status'		=> 2,
					'co_activedate' => date('Y-m-d H:i:s'),
					'co_expireddate' => date('Y-m-d H:i:s', strtotime($customer_order_detail['co_mo_subs']." month")),
					'co_activatedby'=> $co_activatedby,
				);

                if($co_id)
                {
                    $this->master->db_update('customer_order','co_id',$co_id,$data_update_order);
                }

                else if($transactionId)
                {
                    $this->master->db_update('customer_order','co_num_transidmerchant', $transactionId, $data_update_order);
                }

				//Update password to user table
				$data_update_pass = array(
					'password'	=> md5($password)
				);
				$this->master->db_update('user','username',$cu_name_email,$data_update_pass);

                $res = [
                    'status' => 201,
                    'error' => null,
                    "messages"  => [
                        "messages"  => 'Account '.$companyName.' with login '.$response['login'].' Created on VS and Updated on customer order table!'   
                    ]
                ];
                return $this->respondCreated($res);
            }


        // } catch (\Throwable $th) {
        //     return $this->fail("Something Wrong When Updating to DB", 500);;
        // } 

    }

    public function create_pbx_account_pd($data, $transactionID = null)
    {
        $companyName = $data['companyName'];
        $cu_name_email = $data['cu_name_email'];
        $password = $data['password'];
        
        // Verify if Required params are provided
        if (!$companyName || !$cu_name_email || !$password)
        {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'create_pbx_account: companyName, cu_name_email, and password cannot be empty!');
            echo json_encode('companyName, cu_name_email, and password cannot be empty!');
            die();
        }

        // Verify if cu_name_email exist in User table
        $user_data = $this->user->where(['username' => $cu_name_email])->first();
        if(!isset($user_data))
        {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'create_pbx_account: username with cu_name_email not exist!');
            echo json_encode('username with cu_name_email not exist!');
            die();
        }

        // Verify if customer order with given co_id or transactionId is exist
        $customer_order_detail = null;
        $transactionId = $data['transactionId'];

        if($transactionId)
        {
            $customer_order_detail = $this->master->get_row_param('customer_order','co_num_transidmerchant', $transactionId);
        }
        
        if(!isset($customer_order_detail))
        {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'create_pbx_account: Order with co_id or transactionId is not Exist!');
            echo json_encode('Order with co_id or transactionId is not Exist!');
            die();
        }
        
        // Prepare Json data to be sent to webAPI
        $json = array(
            'companyName'   => $companyName,
            'login' => str_replace('@', '_', $data['cu_name_email']),
            'password'      => $password,
            'webPassword'   => $password,
            'address'   => $data['address'],
            'zipCode'   => $data['ca_zipcode'],
            'city'  => $data['city'],
            'taxID' => "PPN",
            'eMail' => $data['cu_name_email'],
            'phoneNumber'   => $data['phoneNumber'],
            'mobileNumber'  => $data['mobileNumber'],
            'tariffId'      => 111,
            'accountState'  => 1,
            'generateInvoice'   => false,
            'sendInvoice'   => false
        );

        $sendURL = $this->webAPIBaseURL.'/json/syncreply/AdminCreatePBXAccount';

        try {
            $response = $this->curl->content_gc_basic_auth(
                $sendURL,
                $json,
                'POST',
                $this->userName,
                $this->passwordHash
            );

        } catch (\Throwable $th) {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'create_pbx_account: Something Wrong When Send WebAPI');
           echo json_encode("Something Wrong When Send WebAPI");
           die();
        }
            

        try{
            if(isset($response['responseStatus']))
            {
                $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'create_pbx_account: error response status from webAPI');
                echo json_encode($response['responseStatus']);
                die();
            }

            else {
                 // Update datas to customer_order table
                
                $co_activatedby = $data['activated_by'];
                 

                $data_update_order = array(
                    'idClient'  => $response['idClient'],
                    'idCompany' => $response['idCompany'],
                    'login'     => $response['login'],
                    'cu_name_email' => $cu_name_email,
                    'co_num_inv'    => 'INV',
                    'co_status'     => 2,
                    'co_activedate' => date('Y-m-d H:i:s'),
                    'co_expireddate' => date('Y-m-d H:i:s', strtotime($customer_order_detail['co_mo_subs']." month")),
                    'co_activatedby'=> $co_activatedby,
                );

                if($transactionId)
                {
                    $this->master->db_update('customer_order','co_num_transidmerchant', $transactionId, $data_update_order);
                }

                //Update password to user table
                $data_update_pass = array(
                    'password'  => md5($password)
                );
                $this->master->db_update('user','username',$cu_name_email,$data_update_pass);

                $res = [
                    'status' => 201,
                    'error' => null,
                    "messages"  => [
                        "messages"  => 'Account '.$companyName.' with login '.$response['login'].' Created on VS and Updated on customer order table!'   
                    ]
                ];
                echo json_encode($res);
                
            }
        
        } catch (\Throwable $th) {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'create_pbx_account: something wrong when update local DB');
            die();
        }
    }
    public function add_balance()
    {
        $money = $this->request->getVar('money');
        $login = $this->request->getVar('login');

        // Set Default Descripion
        $description = $this->request->getVar('description'); 
        if(!isset($description))
        {
            $description = 'Add balance from bucketapi';
        }

        // Verify if money params provided
        $money = (int) $money;
        if ($money < 1)
        {
            return $this->fail('Provide Money!');
        }

        // Verify if login exist and have idCompany, both at local DB and voipswitch DB
        $companyidURL = $this -> webAPIBaseURL. '/json/syncreply/admin.pbx.companyid.get';

        $json = array(
            "login"	=> $login
        );

        $webapi_response= $this->curl->content_gc_basic_auth($companyidURL,$json,'POST',$this->userName,$this->passwordHash);
        $customer_order = $this->customerOrder->where(['login' => $login])->first();

        // Check if login exist on customer_order table and voipswitch's DB
        if (!isset($customer_order) || isset($webapi_response['responseStatus']))
        {
            return $this->fail('login not exist at local DB AND voipswicth DB!');
        }

        // Check if account activated
        if ($customer_order['co_status'] != 2)
        {
            return $this->fail('Activate the Account!');
        }

        // Check if idCompany on customer_order table and voipswitch's DB is same
        $customer_order_idCompany = $customer_order['idCompany'];
        $webapi_idCompany = $webapi_response['idCompany'];

        if ($webapi_idCompany != $customer_order_idCompany)
        {
            return $this->fail('idCompany at local DB AND voipswicth DB is not same!');
        }

        // Post to webAPI
        try {
            $json = array(
                "money"	=> $money,
                "paymentType"	=> 'Prepaid',
                "idClient"		=> $webapi_idCompany,
                "clientType"	=> 128,
                "addToInvoice"	=> false,
                "description"	=> $description,
            );
    
            $sendURL = $this->webAPIBaseURL.'/json/syncreply/admin.payment.add';
        
            $data = $this->curl->content_gc_basic_auth(
                $sendURL,
                $json,
                'POST',
                $this->userName,
                $this->passwordHash,
            );
    
            $response = [
                'status' => 201,
                'error' => null,
                "data" => $data,
                "messages"  => [
                    "message"  => $money.' Balance added to login '.$login
                ],
            ];
            return $this->respondCreated($response);

        } catch (\Throwable $th) {
            return $this->fail("Something Wrong When Post to WebAPI");
        }	
    }
    public function add_balance_pd($data, $transactionID = null)
    {
        $money = $data['money'];
        $login = $data['login'];

        // Set Default Descripion
        $description = $data['description']; 
        if(!isset($description))
        {
            $description = 'Add balance from bucketapi';
        }

        // Verify if money params provided
        $money = (int) $money;
        if ($money < 1)
        {
            echo json_encode('Provide Money!');
            die();
        }

        // Verify if login exist and have idCompany, both at local DB and voipswitch DB
        $companyidURL = $this -> webAPIBaseURL. '/json/syncreply/admin.pbx.companyid.get';

        $json = array(
            "login" => $login
        );

        $webapi_response= $this->curl->content_gc_basic_auth($companyidURL,$json,'POST',$this->userName,$this->passwordHash);
        $customer_order = $this->customerOrder->where(['login' => $login])->first();

        // Check if login exist on customer_order table and voipswitch's DB
        if (!isset($customer_order) || isset($webapi_response['responseStatus']))
        {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'add_balance: login not exist');
            echo json_encode('login not exist at local DB AND voipswicth DB!');
            die();
        }

        // Check if account activated
        if ($customer_order['co_status'] != 2)
        {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'add_balance: account not activated');
            echo json_encode('Activate the Account!');
            die();
        }

        // Check if idCompany on customer_order table and voipswitch's DB is same
        $customer_order_idCompany = $customer_order['idCompany'];
        $webapi_idCompany = $webapi_response['idCompany'];

        if ($webapi_idCompany != $customer_order_idCompany)
        {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP'. 'add_balance: idCompany not same');
            echo json_encode('idCompany at local DB AND voipswicth DB is not same!');
            die();
        }

        // Post to webAPI
        try {
            $json = array(
                "money" => $money,
                "paymentType"   => 'Prepaid',
                "idClient"      => $webapi_idCompany,
                "clientType"    => 128,
                "addToInvoice"  => false,
                "description"   => $description,
            );
            $sendURL = $this->webAPIBaseURL.'/json/syncreply/admin.payment.add';
        
            $data = $this->curl->content_gc_basic_auth(
                $sendURL,
                $json,
                'POST',
                $this->userName,
                $this->passwordHash,
            );
    
            $response = [
                'status' => 201,
                'error' => null,
                "data" => $data,
                "messages"  => [
                    "message"  => $money.' Balance added to login '.$login
                ],
            ];
            echo json_encode($response);
        } catch (\Throwable $th) {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'add_balance: something wrong when post to webAPI');
            echo json_encode("Something Wrong When Post to WebAPI");
            die();
        }   
    }

    public function update_subtrunk()
	{
        $login = $this->request->getVar('login');
        $maxsub = $this->request->getVar('maxsub');
        $trunkline = $this->request->getVar('trunkline');

        // Check Params
        if(!$login || !$trunkline || !$maxsub) {
            return $this->fail('login, maxsub, and trunkline are required!');
        }

        // Check if login exist on customer_order table and voipswitch's DB
        $customer_order = $this->customerOrder->where(['login' => $login])->first();
        if (!isset($customer_order))
        {
            return $this->fail('login not found at local DB!');
        }

        // Check if account activated
        if ($customer_order['co_status'] != 2)
        {
            return $this->fail('Activate the Account!');
        }

        // check if login exist in voipswitch DB and get its id
        $data = $this->subtrunk->where(['login' => $login])->first();
        if(!isset($data))
        {
            return $this->failNotFound('Account with login '.$login.' not found!');
        }
        else if (isset($data["id"])){
            $id = $data["id"];
        
            try {
                // Add New maxclients and trunkline value to account_data
                $account_data = [
                    'MaxClients' => $maxsub,
                    'type2' => $trunkline,
                ];
            
                $this->subtrunk->update($id, $account_data);
            
                $response = [
                    'status' => 200,
                    'error' => null,
                    'messages' => [
                        'message' => $maxsub." Max Sub Account and ".$trunkline." Trunk Line updated to login ".$login
                    ]
                ];
                return $this->respond($response);
            
            } catch (\Throwable $th) {
                return $this->fail('Something Wrong', 500);
            }
        }

        return $this->fail('Something Wrong', 500);
        
	}

    public function update_subtrunk_pd($data, $transactionID = null)
    {
        $login = $data['login'];
        $maxsub = $data['maxsub'];
        $trunkline = $data['trunkline'];

        // Check Params
        if(!$login || !$trunkline || !$maxsub) {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'update_subtrunk: login, maxsub, and trunkline are required!');
            echo json_encode('login, maxsub, and trunkline are required!');
            die();
        }

        // Check if login exist on customer_order table and voipswitch's DB
        $customer_order = $this->customerOrder->where(['login' => $login])->first();
        if (!isset($customer_order))
        {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'update_subtrunk: login not found at local DB');
            echo json_encode('login not found at local DB!');
           die();
        }

        // Check if account activated
        if ($customer_order['co_status'] != 2)
        {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'update_subtrunk: account not activated');
            echo json_encode('Activate the Account!');
            die();
        }

        // check if login exist in voipswitch DB and get its id
        $data = $this->subtrunk->where(['login' => $login])->first();
        if(!isset($data))
        {
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'update_subtrunk: login not found on VS DB');
            echo json_encode('Account with login '.$login.' not found!');
            die();
        }
        else if (isset($data["id"])){
            $id = $data["id"];
        
            try {
                // Add New maxclients and trunkline value to account_data
                $account_data = [
                    'MaxClients' => $maxsub,
                    'type2' => $trunkline,
                ];

                $this->subtrunk->update($id, $account_data);
            
                $response = [
                    'status' => 200,
                    'error' => null,
                    'messages' => [
                        'message' => $maxsub." Max Sub Account and ".$trunkline." Trunk Line updated to login ".$login
                    ]
                ];
                echo json_encode($response);
            
            } catch (\Throwable $th) {
                $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'update_subtrunk: something wrong when updating to VS DB');
                echo json_encode('Something Wrong');
                die();
            }
        }

        else{
            $this->emailNotification($transactionID, 'failed_activation', 'Gagal Aktivasi Akun PBX/SIP', 'update_subtrunk: error checking login on VS DB');
            echo json_encode('Something Wrong');
            die();
        }
    }

    public function show_subtrunk()
	{
        // PARAMS:
        // login
        
        $login = $this->request->getVar('login');
        
        try {
            $data = $this->subtrunk->where(['login' => $login])->first();
            // $data = $this->subtrunk->findAll();
            if ($data) {
                $response = [
                    'status' => 200,
                    'error' => null,
                    'messages' => "Max Sub Account and Trunk Line Found",
                    "data" => $data,
                ];
                return $this->respond($response);
            } else {
                return $this->failNotFound('No Max Sub Account and Trunk Line Found with login ' . $login);
            }
        } catch (\Throwable $th) {
            return $this->fail('Send subtrunk to VS DB failed', 500);
        }
    }

    function emailNotification($transactionID = '[NOT AVAILABLE]', $content = 'failed_activation', $subject = null, $error_msg = null){
		$customerOrderModel = new CustomerOrderModel();
        $customerOrderDetail = $customerOrderModel->where(['co_num_transidmerchant' => $transactionID])->first();

		// Send Invoice Email to Sales
		$url = 'https://bucketapi.voxnet.id/public/mailersmtp/send_sales_notification';
		$data = array(
        	"co_id" => $customerOrderDetail['co_id'],
		    "destination" => 'billing@voxnet.id',
			"content" => $content,
			"subject" => $subject,
            "error_msg" => $error_msg,
        );

        $this->curlModel->content_gc($url,$data,"POST");
	}


    // public function sync_account_not_finished()
    // {
    //     $cu_name_email = $this->request->getVar('cu_name_email');
    //     // Get Account
    //     $passwordHash = sha1($userPasword);
    //     $companyidURL = $this -> baseURL . '/json/syncreply/admin.pbx.companyid.get';
    //     $clientidURL = $this -> baseURL . '/json/syncreply/admin.client.id.get';
    //     $passwordURL = $this -> baseURL . '/json/syncreply/admin.client.password.get';

    //     $json = array(
    //     	"login"	=> $cu_name_login
    //     );

    //     $companyidRespon= $this->curl->content_gc_basic_auth($companyidURL,$json,'POST',$this->userName,$passwordHash);
    //     $clientidRespon	= $this->curl->content_gc_basic_auth($clientidURL,$json,'POST',$this->userName,$passwordHash);

	// 	// 	//jika semua baik-baik saja
	// 	// 	if (!isset($data['companyidRespon']['responseStatus']))
	// 	// 	{
	// 	// 		$coaWhere = array(
	// 	// 			'cc_id'	=> $data['request']->uri->getSegment(4),
	// 	// 			'co_status' => 2,
	// 	// 			'login' => $cu_name_login
	// 	// 		);
	// 	// 		$data['customer_order_cek'] = $this->master->get_row_where_array('customer_order',$coaWhere);

	// 	// 		if ($data['customer_order_cek'])
	// 	// 		{
	// 	// 			$codWhere = array( 'co_id' => $data['request']->uri->getSegment(6) );
	// 	// 			$data['customer_order_detail'] = $this->master->get_row_where_array('customer_order',$codWhere);

	// 	// 			if ($data['customer_order_cek']['login'] != $data['customer_order_detail']['login'])
	// 	// 			{
	// 	// 				$data['notif_syncacc'] = 2;
	// 	// 			}
	// 	// 			else
	// 	// 			{
	// 	// 				$data['notif_syncacc'] = 1;
	// 	// 			}
	// 	// 		}
	// 	// 		else
	// 	// 		{
	// 	// 			$data['notif_syncacc'] = 1;
	// 	// 		}

	// 	// 		if ($data['notif_syncacc'] == 1)
	// 	// 		{
	// 	// 			//Akun sync to tabel customer_order
	// 	// 			$data_update_order = array(
	// 	// 				'idClient'	=> $data['clientidRespon']['idClient'],
	// 	// 				'idCompany'	=> $data['companyidRespon']['idCompany'],
	// 	// 				'login'		=> $data['clientidRespon']['login'],
	// 	// 				'co_num_inv'	=> 'INV',
	// 	// 				'co_status'		=> 2,
	// 	// 				'co_activedate' => $this->request->getPost('co_activedate'),
	// 	// 				'co_mo_subs'	=> $this->request->getPost('co_mo_subs'),
	// 	// 				'co_expireddate'=> $this->request->getPost('co_expireddate'),
	// 	// 				'co_activatedby'=> $session->get('email')
	// 	// 			);
	// 	// 			$this->master->db_update('customer_order','co_id',$data['request']->uri->getSegment(6),$data_update_order);

	// 	// 			//Update password to user table
	// 	// 			$passwordJson = array(
	// 	// 				"clientId"	=> $data['clientidRespon']['idClient'],
	// 	// 				"clientType"=> $data['clientidRespon']['clientType']
	// 	// 			);
	// 	// 			$data['passwordRespon']= $this->curl->content_gc_basic_auth($passwordURL,$passwordJson,'POST',$userName,$passwordHash);

	// 	// 			$data_update_pass = array(
	// 	// 				'password'	=> md5($data['passwordRespon']['password'])
	// 	// 			);
	// 	// 			$this->master->db_update('user','username',$cu_name_email,$data_update_pass);
	// 	// 		}
	// 	// }
    // }
}


    // To activate PBX account + it's package (trunk line, maxsubaccount, credit(balance)),
    // Follow this step:
    // 1. Activate account with activate_pbx_account()
    // 2. Get the response datas (especially idCompany, login, idClient)
    // 3. update customer_order_table:
    //      - 'idClient'	=> $data['respon']['idClient'],
    //      - 'idCompany'	=> $data['respon']['idCompany'],
    //      - 'login'		=> $data['respon']['login'],
    //      - 'cu_name_email'	=> $this->request->getVar('cu_name_email'),
    //      - 'co_num_inv'	=> 'INV',
    //      - 'co_status'		=> 2,
    //      - 'co_activedate' => date('Y-m-d H:i:s'),
    //      - 'co_expireddate' => date('Y-m-d H:i:s', strtotime($data['customer_order_detail']['co_mo_subs']." month")),
    //      - 'co_activatedby'=> $session->get('email')
    // 4. update password at user table
    // 5. using login at customer order table, get account's $id with show_subtrunk()
    // 6, using $id, update maxsubaccount and trunkline with update_subtrunk() 
    // 7. using $idCompany, add talk credit (balance) using add_balance()
    // 8. Send login info to user via mailersmtp API 
