<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

Class Options implements FilterInterface
{
  public function before(RequestInterface $request, $arguments = null)
    {
     
      $origin = (getenv('FRONTEND_ORIGIN')) ? (getenv('FRONTEND_ORIGIN')) : ('http://localhost:3000'); 
      
      header("Access-Control-Allow-Origin: {$origin}");
      header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
      $method = $_SERVER['REQUEST_METHOD'];
      if ($method == "OPTIONS") {
      die();
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
      // Do something here
    }
}