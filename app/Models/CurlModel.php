<?php namespace App\Models;

use CodeIgniter\Model;

class CurlModel extends Model {

	// Menggunakan curl
	public function get_curl($url)
	{
		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($c);
        curl_close($c);

	    return json_decode($response, true);
    }
    
    public function get_curl_basic_auth($url)
	{
		// belum jadi
		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($c);
        curl_close($c);

	    return json_decode($response, true);
	}

	// menggunakan file_get_contents
	public function get_gc($url)
	{
		$userHeaders = "Content-type: application/json" . "\r\n";
		$usersOptions = array(
			'http' => array(
				'header'  => $userHeaders
			),
		);
		$userContext  = stream_context_create($usersOptions);
		$userResult = file_get_contents($url, false, $userContext);

		return json_decode($userResult, true);
	}

	public function content_gc($url,$data,$method)
	{
		$dataJson = json_encode($data);
		$headers = "Content-type: application/json" . "\r\n";
		$headers = $headers . "Content-Length: " . strlen($dataJson) . "\r\n";
		$options = array(
			'http' => array(
				'method'  => $method,
				'header'  => $headers,
				'content' => $dataJson
			),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);

		return json_decode($result, true);
	}
	public function content_gc_post($url,$data,$method){
		$postdata = http_build_query($data);

		$opts = array('http' =>
			    array(
			        'method'  => $method,
			        'header'  => 'Content-Type: application/x-www-form-urlencoded',
			        'content' => $postdata
			    ),
			    "ssl"=>array(
				    "verify_peer"=>true,
				    "verify_peer_name"=>true,
			    ),
		);

		$context  = stream_context_create($opts);
		// $result = file_get_contents($url, false, $context);
		$result = file_get_contents($url, false);
		print_r($result);

	}

	public function content_gc_basic_auth($url,$data,$method,$userName,$passwordHash)
	{
		$dataJson = json_encode($data);
		$authData = base64_encode($userName . ':' . $passwordHash);
		$headers = "Authorization: Basic " . $authData  . "\r\n";
		$headers = $headers . "Content-type: application/json" . "\r\n";
		$headers = $headers . "Content-Length: " . strlen($dataJson) . "\r\n";
		$options = array(
			'http' => array(
				'method'  => $method,
				'header'  => $headers,
				'content' => $dataJson
			),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);

		return json_decode($result, true);
	}
}//EOF class CurlModel extends CI_Model

/* End of file CurlModel.php */
/* Location: ./application/models/CurlModel.php */
