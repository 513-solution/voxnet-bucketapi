<?php

namespace App\Models;

use CodeIgniter\Model;

class CustomerAddressModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'customer_address';
	protected $primaryKey           = 'ca_id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		"cc_id",
		"cac_id",
		"ca_attention",
		"ca_address_1",
		"ca_address_2",
		"ca_lr_id",
		"ca_lp_id",
		"ca_zipcode",
		"ca_fax",
		"ca_country"
	];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
