<?php

namespace App\Models;

use CodeIgniter\Model;

class CustomerOrderAddonModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'customer_order_addon';
	protected $primaryKey           = 'coa_id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		"co_id",
		"pa_id",
		"pam_id",
		"coa_value",
		"coa_price",
		"coa_qty",
		"coa_qty_total",
		"coa_price_total",
		"coa_mo_subs",
	];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
