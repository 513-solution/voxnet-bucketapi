<?php

namespace App\Models;

use CodeIgniter\Model;

class CustomerOrderModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'customer_order';
	protected $primaryKey           = 'co_id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'idClient',
		'idCompany',
		'cu_name_email',
		'login',
		"p_id",
		"cc_id",
		"co_num_ip",
		"co_num_inv",
		"co_num_transdate",
		"co_num_transcode",
		"co_num_transidmerchant",
		"co_name",
		"co_name_sub",
		"co_name_telnumber",
		"co_name_trunknum",
		"co_name_talkcredit",
		"co_name_talkdur",
		"co_name_maxsub",
		"co_name_maxtalkcredit",
		"co_name_avgsubscribe",
		"co_name_monthlysubscribe",
		"co_status",
		"co_mo_subs",
		"co_createdate",
		"co_activedate",
		'co_activatedby',
		'co_expireddate',
	];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
