<?php

namespace App\Models;

use CodeIgniter\Model;

class IbpbxModel extends Model
{
	protected $DBGroup              = 'voipswitchDB';
	protected $table                = 'dialingplan';
	protected $primaryKey           = 'id_dialplan';
	protected $format               = 'json';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		"telephone_number",
		"priority",
		"route_type",
		"tech_prefix",
		"dial_as",
		"id_route",
		"call_type",
		"type",
		"from_day",
		"from_hour",
		"to_hour",
		"balance_share",
		"fields",
		"call_limit",
		"id_dial_plan"
	];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}

