<?php

namespace App\Models;

use CodeIgniter\Model;

class MasterModel extends Model
{
    public function db_update($table,$param,$value,$data)
    {
        $query = $this->db->table($table)
            ->where($param, $value)
            ->update($data);

        return $query;
    }
    
    public function alphanumeric()
    {
        $permittedChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randChars = substr(str_shuffle($permittedChars), 0, 10);

        return $randChars;
    }

    public function get_row_param_join_order($table, $param, $value, $tbjoin, $paramjoin, $column, $ascdsc)
    {
        return $query = $this->db->table($table)
            ->where($param, $value)
            ->join($tbjoin, $paramjoin)
            ->orderBy($column, $ascdsc)
            ->get()
            ->getRowArray();
    }

    public function get_min_where_array($column, $table, $whereArray)
    {
        return $query = $this->db->table($table)
            ->selectMin($column)
            ->where($whereArray)
            ->get()
            ->getRowArray();
    }

    public function get_row_param($table, $param, $value)
    {
        return $query = $this->db->table($table)
            ->where($param, $value)
            ->get()
            ->getRowArray();
    }

    public function get_result_param($table, $param, $value)
    {
        return $query = $this->db->table($table)
            ->where($param, $value)
            ->get()
            ->getResultArray();
    }
}
