<?php

namespace App\Models;

use CodeIgniter\Model;

class PutPBXDialingplan extends Model
{
	protected $DBGroup              = 'voipswitchDB';
	protected $table                = 'pbx_dialingplan';
	protected $primaryKey           = 'id_dialingplan';
	protected $format               = 'json';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		"id_company",
		"telephone_number",
		"priority",
		"route_type",
		"id_route",
		"tech_prefix",
		"call_type",
		"from_day",
		"to_day",
		"from_hour",
		"to_hour",
		"balance_share",
		"properties"
	];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
