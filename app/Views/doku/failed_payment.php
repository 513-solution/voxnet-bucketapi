<?php

use App\Models\MasterModel;

$this->master = new MasterModel();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="icon" type="image/x-icon" href="https://irp-cdn.multiscreensite.com/c35bb19a/site_favicon_16_1607345787223.ico">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400&amp;display=swap" rel="stylesheet" />
	<title>Voxnet | Expired</title>
<style type="text/css">
		body {
			font-family: "DM Sans","sans-serif";
    		background: linear-gradient(
			119.76deg,rgba(231,98,27,.1) 35.95%,rgba(255,136,72,.1) 79%);
		}

		.container {
			text-align: center;
			max-width: 50em;
			padding: 3em;
			margin: auto
		}
		
		.title {
			color: #e7621b;
    		font-size: 2.5em;
    		font-weight: 600;
			margin: 0.8em auto;
		}

		.sub-title {
			color: rgb(3, 147, 62);
            font-size: 1.5em;
            font-weight: 600;
		}

		.bg-white {
			max-width: 60rem;
    		margin-top: 1rem;
    		background-color: #fff;
    		border-radius: 1rem;
    		padding: 3rem;
    		box-shadow: 3px 3px 2px 1px rgb(0 0 0 / 20%);
		}

		.note{
			color: rgb(3, 147, 62);
            font-size: 1.3em;
            font-weight: 400;
		}

</style>
</head>

<body>
	<div class='container'>
		<div class='bg-white'>	
			<div class='sub-title'>Voxnet Doku Payment</div>
			<div class='title'><?= $message ?></div>
			<div class='note'>Jika terdapat kendala, hubungi billing@voxnet.id</div>		
		</div>
	</div>
</body>
<script type="text/javascript">

</script>

</html>

