<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="icon" type="image/x-icon" href="https://irp-cdn.multiscreensite.com/c35bb19a/site_favicon_16_1607345787223.ico">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400&amp;display=swap" rel="stylesheet" />
	<title>Voxnet | Redirect to Doku</title>

	<style type="text/css">

        body {
			font-family: "DM Sans","sans-serif";
    		background: linear-gradient(
			119.76deg,rgba(231,98,27,.1) 35.95%,rgba(255,136,72,.1) 79%);
		}

		.container {
			text-align: center;
			max-width: 50em;
			padding: 3em;
			margin: auto
		}
		
		.title {
			color: #e7621b;
    		font-size: 2.5em;
    		font-weight: 600;
			margin-bottom: 2rem;
		}

		.sub-title {
			color: rgb(3, 147, 62);
            font-size: 1.5em;
            font-weight: 600;
            margin-bottom: 0.8em;
		}

		.bg-white {
			max-width: 60rem;
    		margin-top: 1rem;
    		background-color: #fff;
    		border-radius: 1rem;
    		padding: 3rem;
    		box-shadow: 3px 3px 2px 1px rgb(0 0 0 / 20%);
		}  

	</style>
</head>

<body>
<div class=container>
<div class="bg-white">		
	<div class='sub-title'>Voxnet Payment via Doku</div>
	<div class='title'>Redirecting to Payment Page</div>
	  <div class="spinner-grow text-success spinner-grow-sm" role="status">
		<span class="sr-only"></span>
	  </div>
	  <div class="spinner-grow text-success spinner-grow-sm" role="status">
		<span class="sr-only"></span>
	  </div>
	  <div class="spinner-grow text-success spinner-grow-sm" role="status">
		<span class="sr-only"></span>
	  </div>
</div>
</div>

<form action="<?php echo $URL; ?>" id="MerchatPaymentPage" name="MerchatPaymentPage" method="post" >
<input name="BASKET" type="hidden" id="BASKET" value="<?php echo $BASKET; ?>"/>
<input name="MALLID" type="hidden" id="MALLID" value="<?php echo $MALLID ?>" />
<input name="CHAINMERCHANT" type="hidden" id="CHAINMERCHANT" value="NA" />
<input name="CURRENCY" type="hidden" id="CURRENCY" value="360"/>
<input name="PURCHASECURRENCY" type="hidden" id="PURCHASECURRENCY" value="360"/>
<input name="AMOUNT" type="hidden" id="AMOUNT" value="<?php echo $AMOUNT; ?>" />
<input name="PURCHASEAMOUNT" type="hidden" id="PURCHASEAMOUNT" value="<?php echo $PURCHASEAMOUNT; ?>" />
<input name="TRANSIDMERCHANT" type="hidden" id="TRANSIDMERCHANT" value="<?php echo $TRANSIDMERCHANT; ?>" />
<input name="SHAREDKEY" type="hidden" id="SHAREDKEY" value="<?php echo $SHAREDKEY; ?>">
<input type="hidden" id="WORDS" name="WORDS" value="<?php echo $WORDS; ?>"  />
<input name="REQUESTDATETIME" type="hidden" id="REQUESTDATETIME" value="<?php echo $REQUESTDATETIME; ?>"/>
<input type="hidden" id="SESSIONID" name="SESSIONID" value="<?php echo $SESSIONID ?>" />
<input name="EMAIL" type="hidden" id="EMAIL" value="<?php echo $EMAIL; ?>" />
<input name="NAME" type="hidden" id="NAME" value="<?php echo $NAME; ?>"/>
<input name="ADDRESS" type="hidden" id="ADDRESS" value="<?php echo $ADDRESS; ?>"/>
<input name="COUNTRY" type="hidden" id="COUNTRY" value="360"/>
<!-- <input name="submit" type="submit" class="bt_submit" id="submit" value="Continue Payment" /> -->
</form>
</body>
<script type="text/javascript">
    window.onload=function(){
        
        function submitform(){
          document.forms["MerchatPaymentPage"].submit();
        }
        setTimeout(function(){ submitform();}, 3000);
        
    }
</script>

</html>

