<?php

use App\Models\MasterModel;

$this->master = new MasterModel();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <!-- <meta name="description" content="Voxnet User Register Form" /> -->
    <link rel="icon" type="image/x-icon" href="https://irp-cdn.multiscreensite.com/c35bb19a/site_favicon_16_1607345787223.ico" />
    <title>Voxnet</title>
    <style type="text/css"></style>
</head>
<body>
    <div class='mb-5' style='border-bottom: 1px solid #212529'>

    <h4  style='font-size: 1.1rem;'><strong>Kami menerima permintaan untuk mengingatkan SIP username dan password anda</strong></h4>
        
        <p style='font-size: 1.1rem;' class='mb-0'>SIP Username : <strong><?= $sip_login ?></strong></p>
        <p style='font-size: 1.1rem;' class='mb-7'>SIP Password : <strong><?= $password ?></strong></p>

        <p>Atur langganan anda dengan login ke <a href='https://selfservice.voxnet.id'>https://selfservice.voxnet.id</a></p>
        <p class='mb-0'>Email : <strong><?= $username ?></strong></p>
        <p class='mb-5'>Password : <strong><?= $password ?></strong></p>

        <p>Jika Anda memerlukan informasi lebih lanjut, silahkan hubungi WhatsApp Voxnet di nomor telepon 021-3111-0444 (Hanya Message, tidak Call)</p>
        <ul>
          <li>Email seputar <strong>Product</strong> ke: info@voxnet.id</li>
          <li>email Seputar <strong>pembayaran</strong> ke Billing@voxnet.id</li>
        </ul>
        <p>Jika mengalami kendala pada saat penggunaan akun Voxnet, support@voxnet.id</p>
        <p class='mb-5'>Terima Kasih</p>

    </div>
</body>

</html>