<?php

use App\Models\MasterModel;

$this->master = new MasterModel();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta name="description" content="Voxnet User Register Form" />
    <link rel="icon" type="image/x-icon" href="https://irp-cdn.multiscreensite.com/c35bb19a/site_favicon_16_1607345787223.ico" />
    <title>Voxnet | <?= $doc_title ?></title>
    <style type="text/css">
        
        @font-face {
          font-family: 'DM Sans';
          font-style: normal;
          font-weight: 400;
          src: url(https://fonts.gstatic.com/s/dmsans/v6/rP2Hp2ywxg089UriCZ2IHSeH.woff2) format('woff2');
          unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
          font-family: 'DM Sans';
          font-style: normal;
          font-weight: 400;
          src: url(https://fonts.gstatic.com/s/dmsans/v6/rP2Hp2ywxg089UriCZOIHQ.woff2) format('woff2');
          unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        body {
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
        }
        p {
          margin-top: 0;
          margin-bottom: 1rem;
        }
        h4 {
          font-size: calc(1.275rem + 0.3vw);
        }
        @media (min-width: 1200px) {
          .h4,
          h4 {
            font-size: 1.5rem;
          }
        }
        strong {
          font-weight: bolder;
        }
            .row {
          --bs-gutter-x: 1.5rem;
          --bs-gutter-y: 0;
          display: flex;
          flex-wrap: wrap;
          margin-top: calc(var(--bs-gutter-y) * -1);
          margin-right: calc(var(--bs-gutter-x) * -0.5);
          margin-left: calc(var(--bs-gutter-x) * -0.5);
        }
        .row > * {
          flex-shrink: 0;
          width: 100%;
          max-width: 100%;
          padding-right: calc(var(--bs-gutter-x) * 0.5);
          padding-left: calc(var(--bs-gutter-x) * 0.5);
          margin-top: var(--bs-gutter-y);
        }
        .mb-0 {
          margin-bottom: 0 !important;
        }
        .mb-5 {
          margin-bottom: 3rem !important;
        }
        .col {
          flex: 1 0 0%;
        }
          .form-title {
          font-weight: 600;
          font-size: 2em;
          color: #15b558;
          margin: 0 auto 0.6em 0;
        }
        @media (max-width: 991.98px) {
          .table-responsive-lg {
            overflow-x: auto;
            -webkit-overflow-scrolling: touch;
          }
        }
        table {
          caption-side: bottom;
          border-collapse: collapse;
        }
        th {
          text-align: inherit;
          text-align: -webkit-match-parent;
        }
        tbody,
        td,
        tfoot,
        th,
        thead,
        tr {
          border-color: inherit;
          border-style: solid;
          border-width: 0;
        }
        .table {
          --bs-table-bg: transparent;
          --bs-table-accent-bg: transparent;
          --bs-table-striped-color: #212529;
          --bs-table-striped-bg: rgba(0, 0, 0, 0.05);
          --bs-table-active-color: #212529;
          --bs-table-active-bg: rgba(0, 0, 0, 0.1);
          --bs-table-hover-color: #212529;
          --bs-table-hover-bg: rgba(0, 0, 0, 0.075);
          width: 100%;
          margin-bottom: 1rem;
          color: #212529;
          vertical-align: top;
          border-color: #dee2e6;
        }
        .table > :not(caption) > * > * {
          padding: 0.5rem 0.5rem;
          background-color: var(--bs-table-bg);
          border-bottom-width: 1px;
          box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
        }
        .table > tbody {
          vertical-align: inherit;
        }
        .table > thead {
          vertical-align: bottom;
        }
        .table > :not(:last-child) > :last-child > * {
          border-bottom-color: currentColor;
        }
        .text-center {
          text-align: center !important;
        }
        .text-end {
          text-align: right !important;
        }
        .btn {
          display: inline-block;
          font-weight: 400;
          line-height: 1.5;
          color: #212529;
          text-align: center;
          text-decoration: none;
          vertical-align: middle;
          cursor: pointer;
          -webkit-user-select: none;
          -moz-user-select: none;
          user-select: none;
          background-color: transparent;
          border: 1px solid transparent;
          padding: 0.375rem 0.75rem;
          font-size: 1rem;
          border-radius: 0.25rem;
          transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out,
            border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        @media (prefers-reduced-motion: reduce) {
          .btn {
            transition: none;
          }
        }
        .btn:hover {
          color: #212529;
        }

        .btn-danger {
          color: #fff;
          background-color: #dc3545;
          border-color: #dc3545;
        }
        .btn-danger:hover {
          color: #fff;
          background-color: #bb2d3b;
          border-color: #b02a37;
        }
        .btn-check:focus + .btn-danger,
        .btn-danger:focus {
          color: #fff;
          background-color: #bb2d3b;
          border-color: #b02a37;
          box-shadow: 0 0 0 0.25rem rgba(225, 83, 97, 0.5);
        }
        .btn-check:active + .btn-danger,
        .btn-check:checked + .btn-danger,
        .btn-danger.active,
        .btn-danger:active,
        .show > .btn-danger.dropdown-toggle {
          color: #fff;
          background-color: #b02a37;
          border-color: #a52834;
        }
        .btn-check:active + .btn-danger:focus,
        .btn-check:checked + .btn-danger:focus,
        .btn-danger.active:focus,
        .btn-danger:active:focus,
        .show > .btn-danger.dropdown-toggle:focus {
          box-shadow: 0 0 0 0.25rem rgba(225, 83, 97, 0.5);
        }
        .btn-danger.disabled,
        .btn-danger:disabled {
          color: #fff;
          background-color: #dc3545;
          border-color: #dc3545;
        }

        .h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6 {
          margin-top: 0;
          margin-bottom: 0.5rem;
          font-weight: 500;
          line-height: 1.2;
        }
        .float-end {
          float: right !important;
        }
    </style>
</head>

<body style="margin: 0; font-family: DM Sans, sans-serif">
    <div id="root">
        <div class="App">
            <div class="Register" style="max-width: 63rem; margin: 1rem auto">

            <?php if($content === 'failed_did'): ?>
              <div class='mb-5' style='border-bottom: 1px solid #212529'>
                
                <p>Harap Assign DID Number berikut untuk <strong><?=$customer_companies_detail['cc_name']?></strong> dengan akun admin <strong><?= $customer_order_detail['cu_name_email']?></strong></p>
                
                <?php if($customer_order_telephone): ?>
                
                <ul>
                  <?php foreach ($customer_order_telephone as $customer_order_telephone_list) :?>
                    <li><strong><?= $customer_order_telephone_list['cot_telephone']?></strong> - <?=$customer_order_telephone_list['cot_city']?></li>
                  <?php endforeach ?>
                </ul>
                
                <?php endif ?>
                <p>Harap Periksa Order di salesadmin</p>
                <p>Hubungi tim teknis untuk memastikan akun aktif, trunkline, credit, dan subaccount sesuai dengan yang dipesan</p>
                <p class='mb-5'>Terima Kasih</p>
              </div>

            <?php else: ?>
              <div class='mb-5' style='border-bottom: 1px solid #212529'>
                <h4 style='margin-bottom: 2rem;'><strong>Gagal Aktifasi Akun PBX/SIP Trunk</strong></h4>
                <p>Terjadi Kegagalan dalam Proses Aktifasi Akun, Aktifasi Paket, atau Aktifasi DID Number untuk Akun dan Invoice berikut:</p>
                
                <?php if($co_id): ?>
                  <ul>
                    <li><strong>Perusahaan: </strong>: <?= $customer_companies_detail['cc_name'] ?></li>
                    <li><strong>Email Admin: </strong>: <?= $customer_order_detail['cu_name_email']?></li>
                    <li><strong>Invoice: </strong>: <?= $invoice ?></li>
                    <li><strong>Customer order ID (untuk tim IT) </strong>: <?= $co_id ?></li>
                    <li><strong>Error Message (untuk tim IT) </strong>: <?= $error_msg ?></li>
                  </ul>
                <?php endif ?>

                <p>Harap Periksa Order di salesadmin</p>
                <p>Hubungi tim teknis untuk memastikan akun aktif, trunkline, credit, dan subaccount sesuai dengan yang dipesan</p>
                <p class='mb-5'>Terima Kasih</p>
              </div>
              
            <?php endif?>

            <?php if($co_id): ?>
                <form>
                    <div class="row mb-5">
                        <div class="col-md-6">
                            <img src="https://lirp.cdn-website.com/c35bb19a/dms3rep/multi/opt/04+Voxnet+Logo+tanpa+tagline+-+color-128w.png" alt="">
                        </div>
                        <div class="col-md-6">
                            <div class='float-end'>
                                <div class="form-title" style=" font-weight: 600; font-size: 2em; color: #15b558; margin: 0 auto 0.6em 0;">
                                    <?= $doc_title ?>
                                </div>
                                <p class="mb-0">Number: <span style="width:235px; display:inline-block;"><?= $invoice ?></span></p>
                                <p>Period: <span style="width:235px; display:inline-block;"><?= $period ?></span></p>
                            </div>
                        </div>
                    </div>
                    <div class="OrderTable bg-white">
                        <h4><?= $customer_companies_detail['cc_name'] ?></h4>
                        <div class="OrderTable">
                            <p class="mb-0"><?= $customer_address_default['ca_address_1'] ?></p>
                            <p class="mb-0"><?= $customer_address_default['ca_address_2'] ?></p>
                            <p class="mb-0"><?= $customer_address_city['name'] ?>, <?= $customer_address_province['name'] ?></p>
                            <p class="mb-0"><?= $customer_address_default['ca_country'] ?></p>
                            <p><?= $customer_address_default['ca_zipcode'] ?></p>
                        </div>
                        <div class="OrderTable">
                            <p class="mb-0">FAX. <?= $customer_address_default['ca_fax'] ?></p>
                            <p class="mb-0">UP. <?= $customer_address_default['ca_attention'] ?></p>
                            <p>NPWP: <?= $customer_companies_detail['cc_npwp'] ?></p>
                        </div>
                        <div class="OrderTable mb-5">
                            <div class="table-responsive-lg">

                                <table class="table" style="border: 1px solid rgb(222, 226, 230)">
                                    <thead style=" background-color: rgb(3, 147, 62); color: white; border: none; padding: 9px; text-align: center;">
                                        <tr>
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Item</th>
                                            <th class="text-center">Jumlah</th>
                                            <th class="text-center">Harga Satuan (IDR)</th>
                                            <th class="text-center">Total (IDR)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td>
                                                <?= $customer_order_detail['co_name'] ?> - <?= $customer_order_detail['co_name_sub'] ?>
                                                <ul style="padding-left:1rem; font-size:small;">
                                                    <li><?= $customer_order_detail['co_name_telnumber'] ?> Nomor Telepon</li>
                                                    <li><?= $customer_order_detail['co_name_trunknum'] ?> Trunk Line</li>
                                                    <li><?= $customer_order_detail['co_name_talkcredit'] ?> Talk Credits</li>
                                                    <li><?= $customer_order_detail['co_name_maxsub'] ?> Sub Accounts</li>
                                                </ul>
                                            </td>
                                            <td class="text-center">1</td>
                                            <td class="text-center"><?= number_format($customer_order_detail['co_name_monthlysubscribe'], 2) ?></td>
                                            <td class="text-end"><?= number_format($customer_order_detail['co_name_monthlysubscribe'], 2) ?></td>
                                        </tr>
                                        <?php $addUrut = 2; ?>
                                        <?php $subTotalAddon = 0; ?>
                                        <?php $mo_subs = $customer_order_detail['co_mo_subs']; ?>
                                        <?php if(count($customer_order_addon) > 0):?>
                                          <?php foreach ($customer_order_addon as $customer_order_addon_list) : ?>
                                            <?php
                                            $data['product_addon'] = $this->master->get_row_param('product_addon', 'pa_id', $customer_order_addon_list['pa_id']);
                                            if ($customer_order_addon_list['coa_mo_subs'] != null) {
                                                $mo_subs = $customer_order_addon_list['coa_mo_subs'];
                                            }

                                            $subs_total_addon = $customer_order_addon_list['coa_price_total'];
                                            ?>
                                            <tr>
                                                <td class="text-center"><?= $addUrut ?>.</td>
                                                <td><?= $data['product_addon']['pa_name'] ?>: <?= $customer_order_addon_list['coa_value'] ?></td>
                                                <td class="text-center"><?= $customer_order_addon_list['coa_qty'] ?></td>
                                                <td class="text-center"><?= number_format($customer_order_addon_list['coa_price'], 2) ?></td>
                                                <td class="text-end"><?= number_format($customer_order_addon_list['coa_price_total'], 2) ?></td>
                                            </tr>
                                            <?php $addUrut++; ?>
                                            <?php $subTotalAddon += $subs_total_addon; ?>
                                          <?php endforeach ?>
                                        <?php endif ?>
                                        <?php $subTotal = $subTotalAddon + (int)$customer_order_detail['co_name_monthlysubscribe']; ?>
                                        <?php $monthTotal = $subTotal * $mo_subs; ?>
                                        <?php $ppn = 0.1 ?>
                                        <?php $ppnTotal = $monthTotal * $ppn; ?>
                                        <?php $total = $monthTotal + $ppnTotal; ?>

                                        <td colspan="3" rowspan="4" style="font-size:0.8rem; opacity:0.7;"></td>

                                        <tr style=" background-color: rgb(255, 136, 72); color: rgb(255, 255, 255);">
                                            <td class="text-center">Total per Bulan</td>
                                            <td class="text-end"><?= number_format($subTotal, 2) ?></td>
                                        </tr>
                                        <tr style=" background-color: rgb(255, 136, 72); color: rgb(255, 255, 255);">
                                            <td class="text-center">Total <?= $mo_subs ?> bulan</td>
                                            <td class="text-end"><?= number_format($monthTotal, 2) ?></td>
                                        </tr>

                                        <tr style="background-color: rgb(255, 136, 72); color: rgb(255, 255, 255);">
                                            <td class="text-center">PPN 10%</td>
                                            <td class="text-end"><?= number_format($ppnTotal, 2) ?></td>
                                        </tr>
                                        <tr style="background-color: rgb(231, 98, 27); color: rgb(255, 255, 255); font-weight: 600; font-size: 1.1rem;">
                                            <td colspan="3"></td>
                                            <td colspan="1" class="text-center">Total Harga</td>
                                            <td class="text-end" nowrap>Rp. <?= number_format($total, 2) ?></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                       
                          <!-- <div class='mb-5 text-center'>
                            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" itemprop="handler" itemscope="" itemtype="http://schema.org/HttpActionHandler" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                                    <div class="btn-primary" itemprop="url" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: rgb(231, 98, 27); margin: 0; border-color: rgb(231, 98, 27); border-style: solid; border-width: 8px 16px;">
                                      Bayar dengan Doku
                                    </div>
                                </td>
                            </tr>
                          </div> -->

                          <div class="OrderTable mb-5">

                            <table class="table">
                                <tr>
                                    <h4>Pembayaran Manual</h4>
                                </tr>
                                <tr>
                                    <td>Nama Bank</td>
                                    <td>: Bank Mandiri</td>
                                </tr>
                                <tr>
                                    <td>Cabang</td>
                                    <td>: Cabang Jakarta Pasar Rebo, Jakarta Timur, Indonesia</td>
                                </tr>
                                <tr>
                                    <td>Nomor Rekening</td>
                                    <td>: 129-00-10402143</td>
                                </tr>
                                <tr>
                                    <td>Account Name</td>
                                    <td>: PT. Lima Satu Tiga Global Tel-Access</td>
                                </tr>
                            </table>

                            <p style="font-size: 1.1em;">Mohon sertakan <strong>bukti pembayaran</strong> dengan mengirimkan email ke <strong>billing@voxnet.id</strong></p>

                          </div>

                        <div class="OrderTable mb-5">

                            <p>Terima kasih telah menggunakan jasa layanan dari PT. Lima Satu Tiga Global Tel-Access.</p>

                            <p style="margin-bottom:60px;"><strong>Hormat Kami</strong></p>
                            <p class="mb-0"><strong>Dewi Desnal</strong></p>
                            <p class="mb-0">PT. Lima Satu Tiga Global Tel-Access</p>
                            <p class="mb-0">JL.TB.Simatupang No.22 RT.001 RW.04, Rambutan, Jakarta Timur</p>
                            <p>Telp : +62-21-3111-0444</p>
                            <p style="font-size:small;"><?= $doc_footer ?></p>

                        </div>
                    </div>
                </form>
            <?php endif ?>                                
            </div>
        </div>
    </div>
</body>

</html>