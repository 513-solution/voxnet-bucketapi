# ADMIN API

location: `app/Controller/Admin.php`

This controller includes API for admin-level action, such as delete customer's data inside all tables
<br></br>

## CHECK USER PERMISSION

url: `none`

check_user_permission() function used for checking who access the APIs.
It accept $username and $password as argument and check it on User Table. return permission

Note: On The beginning of Admin APIs, make sure that only user with permission '1' can access the APIs.
<br></br>

## DELETE ALL CUSTOMER DATA(S)

Accept cc_id as params and delete all datas related to the cc_id inside following tables:

- customer_companies
- customer_user
- customer_address
- User
- customer_order
- customer_order_addon
- customer_order_telephone

URL: `[BASE_URL]/Admin/delete_customer`

METHOD: `DELETE`

PARAMS:

```
{
    username: string, REQUIRED
    password: string, REQUIRED
    cc_id: string, REQUIRED
}
```

- `username` : username inside User Table with permission 1

- `password` : password of the username (not encrypted)

- `cc_id` : id of customer company you want to delete. fill with 'last' if you want to delete last added customer company
