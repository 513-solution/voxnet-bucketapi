# MAILER SMTP

location: `app/Controllers/MailerSmtp.php`

this controller includes all APIs to send email, especially for sending proforma invoice, invoice, and user login info.

depending on 'template' param, you can choose 1 from 3 types of email template (PI, Invoice, or Credential). All Email templates View are defined at `app/Views/mailer/mailer_invoice.php` (yes, one php view for all templates).

Subject for each templates are defined inside the controller, under `$subject` variable.

the email will be send from noreply@voxnet.id

future API regarding mailing will be added here.
<br></br>

## Send Proforma Invoice via Email

Send Proforma Invoice (PI) of a customer's order (represented from co_id) to destination email adddress. The PI will be rendered into email's body

`URL` : [BASE_URL]/mailersmtp/send_invoice

`METHOD`: POST

`PARAMS` :

```
{
    template: 'pi', REQUIRED,
	destination: string, REQUIRED,
	co_id: string, REQUIRED,
}
```

- `template` : email's body template. 'pi' for proforma invoice

- `co_id` : customer order id that will be rendered into PI on the email's body

- `destination`: destination email address
  <br></br>

### Response Example:

success: email sent

```
{
    "status": 200,
    "error": null,
    "messages": "pi email sent to test_account@gmail.com"
}
```

fail: email failed to sent

```
{
    "status": 500,
    "error": 500,
    "messages": {
        "error": "Something Wrong When Sending Email"
    }
}
```

fail: incorrect destination email format

```
{
    "status": 400,
    "error": 400,
    "messages": {
        "error": "Incorrect Destination Email Format!"
    }
}
```

## Send Invoice via Email

Send invoice of a customer's order (represented from co_id) to destination email adddress. The invoice will be rendered into email's body

`URL` : [BASE_URL]/mailersmtp/send_invoice

`METHOD`: POST

`PARAMS`:

```
{
    template: 'invoice', REQUIRED
	destination: string, REQUIRED,
	co_id: string, REQUIRED,
}
```

- `template` : email's body template. 'invoice' for invoice body template

- `co_id` : customer order id that will be rendered into invoice on the email's body

- `destination`: destination email address
  <br></br>

## Send User's Login Info (Credential) for The First Time via Email

Send user credential (sip_login, username, password) for login to SIP Account and selfserivce portal, and as needed, invoice

Typically we need to send login info to user after PBX/SIP account has been activated for the first time.

Send sip_login and password for login to SIP Account, and also username and password for login to selfservice portal. both password are same

password for login to SIP Account and selfservice portal are same.

`URL` : [BASE_URL]/mailersmtp/send_invoice

`METHOD`: POST

`PARAMS`:

```
{
    template: 'credential', REQUIRED
	destination: string, REQUIRED,
    sip_login: string, REQUIRED
    username: string, REQUIRED,
    password: string, REQUIRED
	co_id: string
}
```

- `template` : email body's template. 'credential' for login information template

- `destination` : destination email address

- `sip_login` : sip username for login to VUC/VUP, IP Phone, and Softphone

- `username` : user's username for login to selfservice (user table -> username)

- `password` : user's password for login to selfservice, VUC/VUP, IP Phone, and Softphone. Make sure it's raw password, not the encrypted one

- `co_id` : not mandatory. If included, the body will render invoice

note: feature to attach User Manual pdf will be added soon

<br></br>

## Re-Send User Login Credetial via Email

Send user credential (sip_login, username, password) for login to SIP Account and selfserivce portal.

Usually when user forget their credential, we need to resend it.

Send sip_login and password for login to SIP Account, and also username and password for login to selfservice portal. both password are same

`URL` : [BASE_URL]/mailersmtp/send_credential

`METHOD`: POST

`PARAMS`:

```
{
	destination: string, REQUIRED,
    sip_login: string, REQUIRED
    username: string, REQUIRED,
    password: string, REQUIRED
}
```

- `destination` : destination email address

- `sip_login` : sip username for login to VUC/VUP, IP Phone, and Softphone

- `username` : user's username for login to selfservice (user table -> username)

- `password` : user's password for login to selfservice, VUC/VUP, IP Phone, and Softphone. Make sure it's raw password, not the encrypted one
