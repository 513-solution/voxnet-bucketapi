# VOIPSWITCH BRIDGE API

location: `app/Controller/Voipswitch.php`

this controllers includes all APIs to interact with voipswitch via WebAPI or directly to voipswitch's database.

API List:

- Activate PBX Account
- Add Account's Balance (Talk Credit)
- Show Account's Trunk
- Update Account's trunk and max subaccount
- Assign DID Number

<br></br>

### Configuration

Voipswitch WebAPI server and Database configured on /.env

```
# ---------- LOGIN ADMIN -------------
 voipswitchUserName = [USERNAME]]#admin
 voipswitchUserPassword = [PASSWORD]


# ----- DEVELOPMENT SERVER & DB ---------
voipswitchWebAPIBaseURL = [VOIPSWITCH DEVELOPMENT WEBAPI BASE URL]
database.voipswitchDB.hostname = [VOIPSWITCH DEVELOPMENT DATABASE IP ADDRESS]

# ----- PRODUCTION SERVER & DB ----------
# voipswitchAPIBaseURL = [VOIPSWITCH PRODUCTION WEBAPI BASE URL]
# database.voipswitchDB.hostname = [VOIPSWITCH PRODUCTION DATABASE IP ADDRESS]

database.voipswitchDB.database = [DATABASE]
database.voipswitchDB.username = [USERNAME]
database.voipswitchDB.password = [PASSWORD]
database.voipswitchDB.DBDriver = MySQLi
```

<br></br>

## Create PBX Account

Send POST Request to voipswitch webAPI (adminCreatePBXAccount) to create new PBX Account. Then, update customer_order and user tables on local bucketapi database

companyName and cu_name_email should unique and not exist on voipswitch's DB.

co_id OR transactionId are reuired to update customer_order table.
if both co_id AND transactionId included on params, co_id will be prioritized.

`URL` : [BASE_URL]/Voipswitch/create_pbx_account

`METHOD`: POST

`PARAMS` :

```
{
    companyName : string, UNIQUE, REQUIRED
    cu_name_email: string, UNIQUE, REQUIRED
    password: REQUIRED
    co_id : int, REQUIRED (if $transactionId not provided)
    transactionId : string, REQUIRED (if $co_id not provided)
    address : string
    ca_zipcode : string
    city : string
    phoneNumber : string
    mobileNUmber : string
}
```

- companyName : customer_company table => cc_name
- cu_name_email : customer_user table => cu_name_email (first user, admin of the account)
- password : raw password
- transactionId : customer_order table => co_num_transidmerchant

### Response Examples:

success: Account successfully activated and database updated

```
{
    "status": 201,
    "error": null,
    "messages": {
        "messages": "Account PT AKTIVASI AKUN with login thoriyama_gmail.com Created on VS and Updated on customer order table!"
    }
}
```

failed: transactionId not exist

```
{
    "status": 400,
    "error": 400,
    "messages": {
        "error": "Order with co_id or transactionId is not Exist!"
    }
}
```

failed: companyName not unique

```
{
    "status": 400,
    "error": 400,
    "messages": {
        "errorCode": "ClientValidateException",
        "message": "Probably client is already in database!"
    }
}
```

<br></br>

## Add Balance to PBX Account

add balance to PBX Account with voipswitch webAPI (admin.payment.add).

accept login to get corresponding idCompany from local database.
This api also check if login's idCompany on local database is same as idCompany at voipswicth DB. return fail response if not same

`URL` : [BASE_URL]/Voipswitch/add_balance

`METHOD`: POST

`PARAMS` :

```
{
    login : string, REQUIRED
    money : int, REQUIRED
    description : string
}
```

- `login` : customer_order_table => login
- `money` : total talk credit (package talk credit + addon talk credit + bonus talk credit)
- `description` : for description on voipswicth's payment history (default: "Add balance from bucketapi")

### Response Examples:

success: balance added

```
{
    "status": 201,
    "error": null,
    "data": {
        "accountState": 667067,
        "creditAllowed": 0,
        "creditBalance": 667067
    },
    "messages": {
        "message": "4000 Balance added to login test_account_mail.com"
    }
}
```

failed: money not provided on params or incorrect money value (less than 1)

```
{
    "status": 400,
    "error": 400,
    "messages": {
        "error": "Provide Money!"
    }
}
```

failed: login not exist at local DB and voipswitch DB

```
{
    "status": 400,
    "error": 400,
    "messages": {
        "error": "login not exist at local DB AND voipswicth DB!"
    }
}
```

failed: account is not activated (co_status != 2)

```
{
    "status": 400,
    "error": 400,
    "messages": {
        "error": "Activate the Account!"
    }
}
```

failed: idCompany at local DB and voipswicth is not same

```
{
    "status": 400,
    "error": 400,
    "messages": {
        "error": "idCompany at local DB AND voipswicth DB is not same!"
    }
}
```

<br></br>

## Update PBX Account's Max SubAccount dan trunkline

update PBX Account's max Subaccount and trunkline value at voipswitch DB.

the login must exist both at local DB and voipswitch DB

the account must be activated (co_status == 2)

`URL` : [BASE_URL]/Voipswitch/update_subtrunk

`METHOD`: POST

`PARAMS` :

```
{
    login: string, REQUIRED,
    maxsub: int, REQUIRED,
    trunk: int, REQUIRED,
}
```

- `login` : customer_order table => login
- `maxsub` : new maximum subaccount value
- `trunkline` : new trunk line value

### Response Examples:

success: max subaccount and trunkline added

```
{
    "status": 200,
    "error": null,
    "messages": {
        "message": "17 Max Sub Account and 17 Trunk Line Added to login test_account_mail.com"
    }
}
```

failed: login, subtrunk, and maxsub params is null

```
{
    "status": 400,
    "error": 400,
    "messages": {
        "error": "login, maxsub, and trunkline are required!"
    }
}
```

failed: login not found on local DB

```
{
    "status": 400,
    "error": 400,
    "messages": {
        "error": "login not found at local DB!"
    }
}
```

failed: account not activated (co_status != 2)

```
{
    "status": 400,
    "error": 400,
    "messages": {
        "error": "Activate the Account!"
    }
}
```

<br></br>

## Get PBX Account's data

get PBX Account's data from server 122.49.225.195 DB. Use this API to get account's id to update subtrunk

`URL` : [BASE_URL]/Voipswitch/show_subtrunk

`METHOD`: GET

`PARAMS` :

```
{
    login: string, REQUIRED
}
```

- `login` : PBX Account's SIP login

<br></br>

### Response example:

success: Data found

```
{
    "status": 200,
    "error": null,
    "messages": "Max Sub Account and Trunk Line Found",
    "data": [
        {
            "id": "218",
            "idReseller": "-1",
            "login": "aktivasi_bucket_mail.com",
            "password": "12345678",
            "type": "65537",
            "id_tariff": "111",
            "callsLimit": "1.0000",
            "clientsLimit": "0.0000",
            "tech_prefix": "",
            "identifier": "",
            "Fullname": "",
            "Address": "",
            "City": "",
            "ZipCode": "",
            "Country": "",
            "Phone": "",
            "Email": "",
            "MaxClients": "17",
            "template_id": "-1",
            "TaxID": "0",
            "storageLimit": "0",
            "type2": "17",
            "language": ""
        }
    ]
}
```

- `type2` : trunkline
- `maxClient` : max Subaccount

failed: not found

```
{
    "status": 404,
    "error": 404,
    "messages": {
        "error": "No Max Sub Account and Trunk Line Found with login permisi_gmail.com"
    }
}
```

<br></br>

## Assign DID Number

Add DID Number to activated PBX Account via voipswitch database.
Assign ONE did number per call. then, update did number's status on product_did_number table

This API accept login and did_number. In order the did number can be assigned to login account on voipswitch DB, the did number should meet requirements:

- The DID number should exist on voipswitch DB
- The DID Number's status on voipswitch DB should be 'available'

therefore, in the future, did number datas and status on voispwitch DB should be copied to local DB

To minimized failed did number assignment, this API verify login and did number that can be assigned. The verifications are as follows:

1. the account is activated (co_status == 2) AND exist on voipswitch DB
2. the did number is ordered by login (customer_order_telephone table)
3. the did number status is 'available' or 'cancelled' (product_did_number table)

\*notes: point 2 and 3 will be disabled for development purposes

This APIs has several step to assign DID Number and update local database:

1. login, did_number, and order verifications
2. Assign did number to login data on voipswitch DB
3. Add its inbound dialplan to voipswitch DB
4. Update its Oubound dialroute to voipswitch DB
5. Update the did number status on product_did_number table at local DB

for development purpose, did_number status and login's order verification can be disabled via disable_verification params

`URL` : [BASE_URL]/Voipswitch/assign_did_number

`METHOD`: POST

`PARAMS` :

```
{
    login : string, REQUIRED
    did_number : string, REQUIRED
    disable_verification : bool, (default: false)
}
```

- `login` : customer_order table => login (should exist both at local DB and voipswitch DB)
- `did_number` : customer_order_telephone table => cot_telephone
- `disable_verification` : if set to true, controller don't check if did_number ordered by login, and ignore did_number's status on product_did_number table

* notes: the voipswitch's webAPI require 'area_name', 'area_code', 'country_id', and 'client_type'. This parameters already set by this API (area_name and area_code fetched from product_did_number table).

### Response Example

success: did number added to account

```

{
"status": 200,
"error": null,
"messages": "Did Number 622230000001 Added to login test_account_mail.com"
}

```
